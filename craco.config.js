const path = require("path")
const resolve = (dir) => path.resolve(__dirname, dir)
const { whenProd, getPlugin, pluginByName } = require("@craco/craco")
const CracoLessPlugin = require("craco-less")
// const HtmlWebpackPlugin = require("html-webpack-plugin")
// 环境变量
const dotenv = require("dotenv")
console.log(process.env.NODE_ENV)
// 根据当前的 NODE_ENV 变量加载不同的 .env 文件
const envFilePath =
	process.env.NODE_ENV === "production" ? ".env.production" : ".env.development"
// 加载环境变量
dotenv.config({
	path: path.resolve(__dirname, envFilePath),
})
// gzip压缩插件
const compressionWebpackPlugin = require("compression-webpack-plugin")
// 打包体积预览插件
const webpackBundleAnalyzer =
	require("webpack-bundle-analyzer").BundleAnalyzerPlugin
// 导入less全局变量
const lessVars = require(resolve("src/styles/less/modifyVars"))
const golbalVars = require(resolve("src/styles/less/variables"))
// const { loaderByName, addAfterLoader } = require("@craco/craco")
// 该配置会和webpack配置整合
// 声明cdn
let cdn = {
	js: [],
}
module.exports = {
	style: {
		postcss: {
			plugins: ["postcss-preset-env"],
		},
	},
	plugins: [
		{
			plugin: CracoLessPlugin,
			options: {
				lessLoaderOptions: {
					lessOptions: {
						modifyVars: lessVars,
						javascriptEnabled: true,
						//配置全局less 变量，不需要在使用的地方导入了
						globalVars: golbalVars,
					},
				},
				modifyLessRule(lessRule, context) {
					// You have to exclude these file suffixes first,
					// if you want to modify the less module's suffix
					lessRule.use.push({
						loader: require.resolve("style-resources-loader"),
						options: {
							patterns: [resolve("src/styles/less/theme.less")],
							// 引入多个文件
							// patterns: [resolve("src/styles/less/variables.less"), resolve("src/styles/less/mixins.less")],
							// 引入多个目录下的文件
						},
					})
					// console.log("lessRule", lessRule.use)
					return lessRule
				},
			},
		},
	],

	// 代理服务
	devServer: {
		port: 8080,
		proxy: {
			[process.env.REACT_APP_ACL_API]: {
				target: process.env.REACT_APP_ACL_SERVE,
				changeOrigin: true,
				pathRewrite: (path) => path.replace(/^\/api/, ""),
			},
			[process.env.REACT_APP_BASE_API]: {
				target: process.env.REACT_APP_BASE_SERVE,
				changeOrigin: true,
				pathRewrite: (path) => path.replace(/^\/api/, ""),
			},
		},
	},
	webpack: {
		// 路径别名
		alias: {
			"@": path.resolve(__dirname, "src"),
			"@/": resolve("src/"),
		},
		configure: (webpackConfig, { env }) => {
			/* addAfterLoader(webpackConfig, loaderByName("sass-loader"), {
				// 配置sass全局变量，如果配置完使用全局变量报错，说明包没下好
				loader: require.resolve("sass-resources-loader"),
				options: {
					resources: [resolve("src/styles/sass/variables.scss")],
				},
			}) */
			// source-map
			webpackConfig.devtool = "source-map"
			webpackConfig.entry = resolve("src/index.tsx")
			webpackConfig.output = Object.assign({}, webpackConfig.output, {
				filename: "js/[name][chunkhash:5].js",
				chunkFilename: "js/[name].[chunkhash:5].chunk.js",
				assetModuleFilename: "static/[name].[hash][ext]",
			})
			webpackConfig.module.rules.push({
				// ... other config
				test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
				use: [
					{
						loader: "@svgr/webpack",
						options: {
							babel: false,
							icon: true,
						},
					},
				],
			})
			// webworkerloader
			webpackConfig.module.rules.push({
				test: /\.worker\.(ts|js)$/, // 匹配以.worker.js结尾的文件
				use: { loader: "worker-loader" }, // 使用worker-loader处理这些文件
				include: [resolve("src")], // 只处理src目录下的文件
			})
			// console.log(webpackConfig.module.rules)
			webpackConfig.resolve.extensions = [
				...webpackConfig.resolve.extensions,
				...[".ts", ".tsx"],
			]

			// webpackConfig.stats = {
			// 	...webpackConfig.stats,
			// 	children: true,
			// }

			// 指定html模板
			const { isFound, match } = getPlugin(
				webpackConfig,
				pluginByName("HtmlWebpackPlugin"),
			)
			if (isFound) {
				match.options.template = path.resolve(__dirname, "public/index.html") // 指定自定义HTML模板路径
				match.options.cdn = cdn
			}
			/* const htmlPluginIndex = webpackConfig.plugins.findIndex(
				(plugin) => plugin.constructor.name === "HtmlWebpackPlugin",
			)

			if (htmlPluginIndex >= 0) {
				const oldHtmlPlugin = webpackConfig.plugins[htmlPluginIndex]
				webpackConfig.plugins.splice(htmlPluginIndex, 1)

				webpackConfig.plugins.push(
					new oldHtmlPlugin.constructor({
						...oldHtmlPlugin.options,
						template: path.resolve(__dirname, "public/index.html"), // 指定自定义HTML模板路径
					}),
				)
			} */
			// console.log(webpackConfig)
			whenProd(() => {
				console.log(webpackConfig.module.rules)
				webpackConfig.devtool = "cheap-module-source-map"
				// gzip
				webpackConfig.plugins.push(
					new compressionWebpackPlugin({
						filename: "[path][name].gz",
						algorithm: "gzip",
						// test: /.js$|.html$|.json$|.css/,
						test: /.js$|.json$|.css$|.ts$|.tsx$|.less/,
						threshold: 10240, // 只有大小大于该值的资源会被处理
						minRatio: 0.8, // 只有压缩率小于这个值的资源才会被处理
						// deleteOriginalAssets: true // 删除原文件
					}),
				)
				// 打包优化
				webpackConfig.optimization = {
					minimize: true,
					// 代码分割配置
					splitChunks: {
						chunks: "all",
						cacheGroups: {
							// layouts通常是admin项目的主体布局组件，所有路由组件都要使用的
							// 可以单独打包，从而复用
							// 如果项目中没有，请删除
							layouts: {
								name: "layouts",
								test: path.resolve(__dirname, "../src/layouts"),
								priority: 40,
							},
							// 如果项目中使用antd，此时将所有node_modules打包在一起，那么打包输出文件会比较大。
							// 所以我们将node_modules中比较大的模块单独打包，从而并行加载速度更好
							// 如果项目中没有，请删除
							antd: {
								name: "chunk-antd",
								test: /[\\/]node_modules[\\/]antd(.*)/,
								priority: 30,
							},
							// 将react相关的库单独打包，减少node_modules的chunk体积。
							react: {
								name: "react",
								test: /[\\/]node_modules[\\/]react(.*)?[\\/]/,
								chunks: "initial",
								priority: 20,
							},
							echarts: {
								name: "echarts",
								test: /[\\/]node_modules[\\/]echarts(.*)?[\\/]/,
								priority: 10,
								chunks: "initial",
							},
							libs: {
								name: "chunk-libs",
								test: /[\\/]node_modules[\\/]/,
								priority: 10, // 权重最低，优先考虑前面内容
								chunks: "initial",
							},
						},
					},
					runtimeChunk: {
						name: (entrypoint) => `runtime~${entrypoint.name}`,
					},
				}
				// cdn 注意，该配置不适用按需加载的三方库
				webpackConfig.externals = {
					react: "React",
					"react-dom": "ReactDOM",
				}
				// 配置现成的cdn资源地址
				// 实际开发的时候 用公司自己花钱买的cdn服务器
				cdn = {
					js: [
						"https://cdnjs.cloudflare.com/ajax/libs/react/18.2.0/umd/react.production.min.js",
						"https://cdnjs.cloudflare.com/ajax/libs/react-dom/18.2.0/umd/react-dom.production.min.js",
					],
				}
				// 通过 htmlWebpackPlugin 插件 在 public/index.html 注入 cdn 资源 url
				const { isFound, match } = getPlugin(
					webpackConfig,
					pluginByName("HtmlWebpackPlugin"),
				)
				if (isFound) {
					// 找到了 HtmlWebpackPlugin 的插件
					match.options.cdn = cdn
					console.log("HtmlWebpackPlugin_Options", match.options)
				}

				// 打包体积分析
				webpackConfig.plugins.push(new webpackBundleAnalyzer())
			})
			return webpackConfig
		},
	},
}
