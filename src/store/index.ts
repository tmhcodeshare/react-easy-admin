import { configureStore, combineReducers } from "@reduxjs/toolkit"
import { useSelector } from "react-redux/es/hooks/useSelector"
import type { TypedUseSelectorHook } from "react-redux/es/types"
// redux持久化
import {
	persistStore,
	persistReducer,
	FLUSH,
	REHYDRATE,
	PAUSE,
	PERSIST,
	PURGE,
	REGISTER,
} from "redux-persist"
// 使用sessionstorage作为存储机制
import storage from "redux-persist/lib/storage/session"
// 使用localstorage作为存储机制
// import storage from "redux-persist/lib/storage"
// 引入所有模块
/* const context = require.context("./modules", false, /\.ts$/)
console.log(context.keys())
let slices: { [key: string]: any } = {}
context.keys().forEach((key) => {
	const module = context(key)
	const name = key.replace(/^\.\/(.*)\.\w+$/, "$1") + "Store"
	console.log(name)
	slices[name] = module.default
})
console.log(slices) */
import countStore from "./modules/count"
import layoutStore from "./modules/layout"
import menusStore from "./modules/menus"
import userStore from "./modules/user"
import categorySore from "./modules/category"
import pdfviewStore from "./modules/pdfview"
// 创建持久化配置
const persistConfig = {
	key: "root",
	version: 1,
	storage,
}
// 整合所有小仓库
const reducer = combineReducers({
	countStore,
	layoutStore,
	menusStore,
	userStore,
	categorySore,
	pdfviewStore,
})
// 创建持久化reducer
const persistReducerConfig = persistReducer(persistConfig, reducer)
// 创建store
const store = configureStore({
	reducer: persistReducerConfig,
	middleware: (getDefaultMiddleware) =>
		getDefaultMiddleware({
			serializableCheck: {
				ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
			},
		}),
})

// 创建持久化 store
export const persistor = persistStore(store)
// 拿到getState返回值的ts类型
export type RootState = ReturnType<typeof store.getState>
// 通过泛型接口传递给useAppSelector函数
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector
export default store
