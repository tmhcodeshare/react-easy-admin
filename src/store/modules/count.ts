import { createSlice } from "@reduxjs/toolkit"
interface Istate {
	count: number
}

const initialState: Istate = {
	count: 0,
}

const countSlice = createSlice({
	name: "countStore",
	initialState,
	reducers: {
		addAction: (state, { payload }) => {
			state.count += payload
		},
	},
})
export const { addAction } = countSlice.actions
export default countSlice.reducer
