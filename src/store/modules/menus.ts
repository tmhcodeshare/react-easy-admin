import { createSlice } from "@reduxjs/toolkit"
interface Tag {
	title: string
	url: string
	closeable: boolean
	icon: string
}
interface Istate {
	tags: Array<Tag>
	current: Tag
}
const initialState: Istate = {
	tags: [
		{ title: "首页", url: "/home", closeable: false, icon: "HomeOutlined" },
	],
	current: {
		title: "首页",
		url: "/home",
		closeable: false,
		icon: "HomeOutlined",
	},
}
const menusSlice = createSlice({
	name: "menusStore",
	initialState,
	reducers: {
		addTagsAciton: (state, { payload }) => {
			if (state.tags.findIndex((it) => it.url === payload.url) !== -1) return
			state.tags.push(payload)
		},
		changeIndexAction: (state, { payload }) => {
			if (state.current.url === payload.url) return
			state.current = payload
		},
		removeTagAction: (state, { payload }) => {
			const index = state.tags.findIndex((it) => it.url === payload.url)
			state.tags.splice(index, 1)
		},
		clearAllTagsAction: (state) => {
			// state.tags = [
			// 	{ title: "首页", url: "/home", closeable: false, icon: "HomeOutlined" },
			// ]
			Object.assign(state, initialState)
		},
	},
})
export const {
	addTagsAciton,
	changeIndexAction,
	removeTagAction,
	clearAllTagsAction,
} = menusSlice.actions
export default menusSlice.reducer
