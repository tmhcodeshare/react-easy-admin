import { createSlice } from "@reduxjs/toolkit"

interface Istate {
	link: string
}

const initialState: Istate = {
	link: "",
}

const pdfviewSlice = createSlice({
	name: "pdfviewStore",
	initialState,
	reducers: {
		setLinkAction: (state, { payload }: { payload: string }) => {
			state.link = payload
		},
	},
})
export const { setLinkAction } = pdfviewSlice.actions

export default pdfviewSlice.reducer
