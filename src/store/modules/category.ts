import { createSlice, createAsyncThunk } from "@reduxjs/toolkit"
import type { RootState } from "@/store"
import {
	reqC1Options,
	reqC2Options,
	reqC3Options,
} from "@/api/productions/attr"
interface IThunkState {
	state: RootState
}
interface CategoryOptions {
	category1Id?: number
	category2Id?: number
	category3Id?: number
	id: number
	name: string
	createTime: string
	updateTime: string
}

interface Istate {
	category1Id: number | string
	category2Id: number | string
	category3Id: number | string
	c1List: CategoryOptions[]
	c2List: CategoryOptions[]
	c3List: CategoryOptions[]
	// loading1: boolean
	// loading2: boolean
	// loading3: boolean
}

const initialState: Istate = {
	category1Id: "",
	category2Id: "",
	category3Id: "",
	c1List: [],
	c2List: [],
	c3List: [],
	// loading1: false,
	// loading2: false,
	// loading3: false,
}
// 一级分类选项菜单
export const fetchC1Options = createAsyncThunk<void, undefined, IThunkState>(
	"c1Options",
	(_, { dispatch }) => {
		reqC1Options()
			.then((res) => {
				if (res.code === 200) {
					dispatch(c1ListAction(res.data))
				}
			})
			.catch((error) => console.log(error))
	},
)
// 二级分类选项菜单
export const fetchC2Options = createAsyncThunk<void, number, IThunkState>(
	"c1Options",
	(id, { dispatch }) => {
		reqC2Options(id)
			.then((res) => {
				if (res.code === 200) {
					dispatch(c2ListAction(res.data))
				}
			})
			.catch((error) => console.log(error))
	},
)
// 三级分类选项菜单
export const fetchC3Options = createAsyncThunk<void, number, IThunkState>(
	"c1Options",
	(id, { dispatch }) => {
		reqC3Options(id)
			.then((res) => {
				if (res.code === 200) {
					dispatch(c3ListAction(res.data))
				}
			})
			.catch((error) => console.log(error))
	},
)

const categorySlice = createSlice({
	name: "categorySore",
	initialState,
	reducers: {
		category1IdAction: (state, { payload }) => {
			state.category1Id = payload
		},
		c1ListAction: (state, { payload }) => {
			state.c1List = payload
		},
		// loading1Action: (state, { payload }) => {
		// 	state.loading1 = payload
		// },
		category2IdAction: (state, { payload }) => {
			state.category2Id = payload
		},
		c2ListAction: (state, { payload }) => {
			state.c2List = payload
		},
		// loading2Action: (state, { payload }) => {
		// 	state.loading2 = payload
		// },
		category3IdAction: (state, { payload }) => {
			state.category3Id = payload
		},
		c3ListAction: (state, { payload }) => {
			state.c3List = payload
		},
		clearAction: (state) => {
			state.c1List = initialState.c1List
			state.c2List = initialState.c2List
			state.c3List = initialState.c3List
			state.category1Id = initialState.category1Id
			state.category2Id = initialState.category2Id
			state.category3Id = initialState.category3Id
		},
		// loading3Action: (state, { payload }) => {
		// 	state.loading3 = payload
		// },
	},
})
export const {
	category1IdAction,
	category2IdAction,
	category3IdAction,
	c1ListAction,
	c2ListAction,
	c3ListAction,
	clearAction,
	// loading1Action,
	// loading2Action,
	// loading3Action,
} = categorySlice.actions
export default categorySlice.reducer
