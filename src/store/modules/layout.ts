import { createSlice } from "@reduxjs/toolkit"
interface Istate {
	fold: boolean // 决定侧边栏是否折叠状态
	showTab: boolean // 是否展示页签导航
	showBread: boolean // 是否展示面包屑
	themeKey: string // 主题
	primary: string
}
const initialState: Istate = {
	fold: false,
	showTab: true,
	showBread: true,
	themeKey: "light",
	primary: "#1677ff",
}
const layoutSlice = createSlice({
	name: "layoutStore",
	initialState,
	reducers: {
		changeFoldAction: (state, { payload }) => {
			state.fold = payload
		},
		changeThemehAction: (state, { payload }) => {
			state.themeKey = payload
		},
		changePrimaryAction: (state, { payload }) => {
			state.primary = payload
		},
		changeShowTabAction: (state, { payload }) => {
			state.showTab = payload
		},
		changeShowBreadAction: (state, { payload }) => {
			state.showBread = payload
		},
		restoreLayoutAction: (state) => {
			// state.fold = initialState.fold
			// state.showTab = initialState.showTab
			// state.showBread = initialState.showBread
			// state.themeKey = initialState.themeKey
			// state.primary = initialState.primary
			Object.assign(state, initialState)
		},
	},
})
export const {
	changeFoldAction,
	changeThemehAction,
	changePrimaryAction,
	changeShowTabAction,
	changeShowBreadAction,
	restoreLayoutAction,
} = layoutSlice.actions
export default layoutSlice.reducer
