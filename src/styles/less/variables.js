// 全局less变量
const golbalVars = {
	"@test-color": "yellow",
	// 定义全局scss变量
	"@color": "rgb(228, 143, 32)",
	// 定义侧边栏宽度
	"@siderbar-width": "220px",
	// 定义侧边栏背景颜色
	"@siderbar-background-color": "#141414",
	// 定义折叠后的侧边栏宽度
	"@siderbar-fold-width": "81px",
	// 定义顶部栏高度
	"@navbar-height": "60px",
	// 定义页签导航高度
	"@tabs-height": "40px",
	// 定义侧边栏logo盒子高度
	"@logo-height": "60px",
	// logo文字颜色
	"@logo-color": "#fff",
	// logo文字大小
	"@logo-font-size": "20px",
	// logo图片大小
	"@logo-img-width": "40px",
	"@logo-img-height": "40px",
	// input边框颜色
	"@input-base-border-color": "#ccc",
	// input聚焦边框颜色
	"@input-focus-border-color": "#409eff",
	// input边框圆角
	"@input-radius": "2px",
}

module.exports = golbalVars
