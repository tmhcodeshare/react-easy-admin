export interface ResultData<T> {
	code: number
	message?: string
	data: T
	ok: boolean
}
