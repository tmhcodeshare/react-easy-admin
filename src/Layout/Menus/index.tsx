import React, { useEffect, useState, memo } from "react"
import * as Icons from "@ant-design/icons"
import type { MenuProps } from "antd"
import { Menu } from "antd"
import type { RouteObject } from "@/router/type"
import { useLocation, useNavigate } from "react-router-dom"
import "./index.less"
import { useAppSelector } from "@/store"
import { shallowEqual } from "react-redux"
import { constanceRoutes } from "@/router/routers"

const LayMenus: React.FC = () => {
	const { fold, dynamicRouteList } = useAppSelector(
		(state) => ({
			fold: state.layoutStore.fold,
			dynamicRouteList: state.userStore.dynamicRouteList,
		}),
		shallowEqual,
	)
	// MenuItem ts类型
	type MenuItem = Required<MenuProps>["items"][number]
	// 动态渲染 Icon 图标
	const customIcons: { [key: string]: any } = Icons
	const addIcon = (name: string) => {
		return React.createElement(customIcons[name])
	}
	// 获取MenuItem方法
	function getItem(
		label: React.ReactNode,
		key?: React.Key | null,
		icon?: React.ReactNode,
		children?: MenuItem[],
		type?: "group",
	): MenuItem {
		return {
			key,
			icon,
			children,
			label,
			type,
		} as MenuItem
	}

	// 整理筛选路由
	const mapMenus = (menus: RouteObject[]): RouteObject[] => {
		return menus
			.map((item) => {
				const { meta, children } = item
				if (!meta?.hiden) {
					if (children && children.length === 1) return children[0]
					return item
				}
			})
			.filter(Boolean) as RouteObject[]
	}

	// 整理展示菜单方法
	const arrangeMenus = (menus: RouteObject[]): MenuItem[] => {
		return menus.map((item) => {
			return getItem(
				item.meta?.title,
				item.path ? item.path : item.meta?.key,
				item.meta?.icon ? addIcon(item.meta?.icon) : null,
				item.children && arrangeMenus(item.children),
			)
		})
	}
	const [items, setItems] = useState<MenuItem[]>([])
	const [current, setCurrent] = useState<string>("home")
	const [openkeys, setOpenkeys] = useState<string[]>([])
	const { pathname } = useLocation()
	const navigate = useNavigate()
	useEffect(() => {
		const dynamicRoutes = JSON.parse(dynamicRouteList)
		if (dynamicRoutes.length > 0) {
			setItems(arrangeMenus(mapMenus(constanceRoutes.concat(dynamicRoutes))))
		}
	}, [dynamicRouteList])
	// 根据路径变化来控制菜单展开项和高亮
	useEffect(() => {
		setCurrent(pathname)
		const arr = pathname.split("/")
		if (arr.length > 2) {
			setOpenkeys([arr[1]])
		} else {
			setOpenkeys([])
		}
	}, [pathname])
	// 点击菜单项回调
	const onClick: MenuProps["onClick"] = (e) => {
		if (e.keyPath.length > 1) {
			const index = e.keyPath.length - 1
			const last = e.keyPath[index]
			// 设置展开项
			setOpenkeys([last])
		} else {
			setOpenkeys([])
		}
		// 设置当前高亮
		setCurrent(e.key)
		// 路由跳转
		navigate(e.key)
	}
	// 点击展开按钮回调
	const openChange = (openKeys: string[]) => {
		if (openKeys.length > 1) {
			const last = openKeys[openKeys.length - 1]
			setOpenkeys([last])
			return
		}
		setOpenkeys(openKeys)
	}
	return (
		<Menu
			className="menuContainer"
			theme="dark"
			onClick={onClick}
			onOpenChange={openChange}
			openKeys={openkeys}
			selectedKeys={[current]}
			inlineCollapsed={fold}
			mode="inline"
			items={items}
		/>
	)
}

export default memo(LayMenus)
