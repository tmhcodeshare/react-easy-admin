import React, { memo, useEffect, useState } from "react"
import {
	Button,
	message,
	Drawer,
	ColorPicker,
	Divider,
	Switch,
	Popover,
} from "antd"
import { shallowEqual, useDispatch } from "react-redux"
import { useAppSelector } from "@/store"
import {
	changePrimaryAction,
	changeThemehAction,
	changeShowTabAction,
	changeShowBreadAction,
} from "@/store/modules/layout"
import {
	FullscreenOutlined,
	SettingOutlined,
	FullscreenExitOutlined,
} from "@ant-design/icons"
import screenfull from "screenfull"
import useLogout from "@/hooks/useLogout"
import "./index.less"

const Setings: React.FC = () => {
	const dispatch = useDispatch()

	const { primary, themeKey, showTab, showBread, userInfo } = useAppSelector(
		(state) => ({
			primary: state.layoutStore.primary,
			themeKey: state.layoutStore.themeKey,
			showTab: state.layoutStore.showTab,
			showBread: state.layoutStore.showBread,
			userInfo: state.userStore.userInfo,
		}),
		shallowEqual,
	)
	const logout = useLogout()
	const [fullScreen, setFullScreen] = useState<boolean>(screenfull.isFullscreen)
	const [open, setOpen] = useState(false)
	useEffect(() => {
		screenfull.on("change", () => {
			if (screenfull.isFullscreen) setFullScreen(true)
			else setFullScreen(false)
			return () => screenfull.off("change", () => {})
		})
	}, [])

	const handleFullScreen = () => {
		if (!screenfull.isEnabled) message.warning("当前您的浏览器不支持全屏 ❌")
		screenfull.toggle()
	}

	const showDrawer = () => {
		setOpen(!open)
	}

	const onClose = () => {
		setOpen(false)
	}

	const style = {
		display: "flex",
		justifyContent: "space-between",
		alignItems: "center",
		marginBottom: "20px",
	}
	return (
		<div className="setingContainer">
			<Button type="link" className="btn" onClick={handleFullScreen}>
				{fullScreen ? (
					<FullscreenExitOutlined style={{ fontSize: "20px" }} />
				) : (
					<FullscreenOutlined style={{ fontSize: "20px" }} />
				)}
			</Button>
			<Button type="link" className="btn" onClick={showDrawer}>
				<SettingOutlined style={{ fontSize: "20px" }} />
			</Button>
			<div className="name">{userInfo?.name}</div>
			<div className="img">
				<Popover
					placement="bottomRight"
					content={
						<Button type="link" className="btn" onClick={logout}>
							退出登录
						</Button>
					}
				>
					<img src={userInfo?.avatar} />
				</Popover>
			</div>
			<Drawer
				title="布局设置"
				placement="right"
				onClose={onClose}
				open={open}
				width={320}
			>
				<Divider>全局主题</Divider>
				<div style={style}>
					<span>暗黑模式</span>
					<Switch
						className="dark"
						defaultChecked={themeKey === "dark"}
						checkedChildren={<>🌞</>}
						unCheckedChildren={<>🌜</>}
						onChange={(checked) =>
							dispatch(changeThemehAction(checked ? "dark" : "light"))
						}
					/>
				</div>
				<div style={style}>
					<span>主题颜色</span>
					<ColorPicker
						showText
						value={primary}
						onChangeComplete={(color) =>
							dispatch(changePrimaryAction(color.toHexString()))
						}
					/>
				</div>
				<Divider className="divider">
					<SettingOutlined />
					界面设置
				</Divider>
				<div style={style}>
					<span>面包屑</span>
					<Switch
						checked={showBread}
						onChange={(checked) => dispatch(changeShowBreadAction(checked))}
					/>
				</div>
				<div style={style}>
					<span>标签栏</span>
					<Switch
						checked={showTab}
						onChange={(checked) => dispatch(changeShowTabAction(checked))}
					/>
				</div>
			</Drawer>
		</div>
	)
}
export default memo(Setings)
