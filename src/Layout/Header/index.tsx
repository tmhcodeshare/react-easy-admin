import React, { memo } from "react"
import "./index.less"
import LayBreadcrumb from "./Breadcrumb"
import Setings from "./Setings"
const LayHeader: React.FC = () => {
	return (
		<header className="headerContainer">
			<div className="left">
				<LayBreadcrumb></LayBreadcrumb>
			</div>
			<div className="right">
				<Setings></Setings>
			</div>
		</header>
	)
}

export default memo(LayHeader)
