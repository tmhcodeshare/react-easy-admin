import { useAppSelector } from "@/store"
import { changeFoldAction } from "@/store/modules/layout"
import React, { useEffect, useState } from "react"
import { shallowEqual, useDispatch } from "react-redux"
import { MenuFoldOutlined, MenuUnfoldOutlined } from "@ant-design/icons"
import { Button, Breadcrumb } from "antd"
import { useLocation, Link } from "react-router-dom"
import type { RouteObject } from "@/router/type"
import { routers } from "@/router"
import "./index.less"
const LayBreadcrumb: React.FC = () => {
	const { pathname } = useLocation()
	const [items, setItems] = useState<Array<any>>([
		{
			title: <Link to="/home">首页</Link>,
		},
	])
	const dispatch = useDispatch()
	useEffect(() => {
		const keys = pathname.split("/").filter(Boolean)
		let arr = findPath([], keys, routers).map((it) => ({ title: it }))
		setItems([
			{
				title: <Link to="/home">首页</Link>,
			},
			...arr,
		])
	}, [pathname])
	const { fold, showBread } = useAppSelector(
		(state) => ({
			fold: state.layoutStore.fold,
			showBread: state.layoutStore.showBread,
		}),
		shallowEqual,
	)
	const findPath = (arr: string[], keys: string[], routers: RouteObject[]) => {
		routers.forEach((it) => {
			if (it.meta && it.meta.key && keys.includes(it.meta?.key)) {
				arr.push(it.meta.title)
				if (it.children) {
					findPath(arr, keys, it.children)
				}
			}
		})
		return arr
	}
	const changeFold = () => {
		dispatch(changeFoldAction(!fold))
	}
	return (
		<div className="breadContainer">
			<Button className="fold" onClick={changeFold}>
				{fold ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />}
			</Button>
			{showBread && <Breadcrumb items={items}></Breadcrumb>}
		</div>
	)
}
export default LayBreadcrumb
