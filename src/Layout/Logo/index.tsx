import React, { memo } from "react"
import setting from "@/setting"
import { useAppSelector } from "@/store"
import { shallowEqual } from "react-redux"
import SvgIcon from "@/components/SvgIcon"
import "./index.less"
const LayLogo: React.FC = () => {
	const { fold } = useAppSelector(
		(state) => ({
			fold: state.layoutStore.fold,
		}),
		shallowEqual,
	)
	return (
		<div className={fold ? "logo fold" : "logo"}>
			<SvgIcon svgName={setting.logo} size="40px" />
			{!fold && <span>{setting.title}</span>}
		</div>
	)
}

export default memo(LayLogo)
