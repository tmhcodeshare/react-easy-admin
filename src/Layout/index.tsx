import React, { memo } from "react"
import { Outlet } from "react-router-dom"
import { useAppSelector } from "@/store"
import { shallowEqual } from "react-redux"
import { Card } from "antd"
import "./index.less"
import LayLogo from "./Logo"
import LayMenus from "./Menus"
import LayHeader from "./Header"
import Tab from "./Tab"
const LayOut: React.FC = () => {
	const { fold, showTab } = useAppSelector(
		(state) => ({
			fold: state.layoutStore.fold,
			showTab: state.layoutStore.showTab,
		}),
		shallowEqual,
	)
	const golbalVars = require("@/styles/less/variables")
	return (
		<div className="layout">
			<div className={`siderbar ${fold ? "fold" : ""}`}>
				<LayLogo></LayLogo>
				<LayMenus></LayMenus>
			</div>
			<div className={`navbar ${fold ? "fold" : ""}`}>
				<LayHeader></LayHeader>
			</div>
			{showTab && (
				<div className={`tab ${fold ? "fold" : ""}`}>
					<Tab></Tab>
				</div>
			)}
			<div
				className={`main ${fold ? "fold" : ""}`}
				style={{
					height: showTab
						? `calc(100vh - ${golbalVars["@navbar-height"]} - ${golbalVars["@tabs-height"]})`
						: `calc(100vh - ${golbalVars["@navbar-height"]}`,
					top: showTab
						? `calc(${golbalVars["@navbar-height"]} + ${golbalVars["@tabs-height"]})`
						: `${golbalVars["@navbar-height"]}`,
				}}
			>
				<Card style={{ height: "100%", overflowY: "auto" }}>
					<Outlet />
				</Card>
			</div>
		</div>
	)
}

export default memo(LayOut)
