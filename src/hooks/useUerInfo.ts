import { useDispatch } from "react-redux"
import {
	saveUserInfoAction,
	saveDynamicRouteListAction,
} from "@/store/modules/user"
import { reqUserInfo } from "@/api/user"
import { filterRoutes, findRoute } from "@/utils"
import { RouteObject } from "@/router/type"
import { useLocation, useNavigate } from "react-router-dom"
import { constanceRoutes, anyRoutes, dynamicRoutes } from "@/router/routers"
import { useEffect, useState } from "react"
const cloneDeep = require("lodash/cloneDeep")
const useGetUserInfo = () => {
	const dispatch = useDispatch()
	const navigate = useNavigate()
	const { pathname } = useLocation()
	const [isFresh, setIsFresh] = useState(false)
	// 路由拦截
	const intercept = (dynamicRouters: RouteObject[], pathname: string) => {
		const routerList = [...constanceRoutes, ...dynamicRouters, ...anyRoutes]
		const route = findRoute(pathname, routerList)
		if (Object.keys(route).length === 0) {
			console.log(1111)
			navigate("/404", { replace: true })
		} else {
			navigate(pathname)
		}
	}
	useEffect(() => {
		console.log(isFresh)

		if (isFresh) {
			reqUserInfo()
				.then((res) => {
					if (res.code === 200) {
						console.log("userinfo", res.data)
						dispatch(saveUserInfoAction(res.data))
						const routes = filterRoutes(
							cloneDeep(dynamicRoutes),
							res?.data?.routes as string[],
						)
						// 触发仓库更新
						dispatch(saveDynamicRouteListAction(JSON.stringify(routes)))
						// 直接传routes，是防止dynamicRouteList还未更新完成，导致未知错误
						intercept(routes, pathname)
					}
				})
				.catch((error) => {
					console.log(error)
				})
				.finally(() => setIsFresh(false))
		}
	}, [isFresh])
	return setIsFresh
}
export default useGetUserInfo
