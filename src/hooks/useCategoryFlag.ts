import { useEffect, useRef, useState } from "react"

const useCategoryFlag = () => {
	let ref = useRef()
	// MainTop2组件是否更新状态 hook
	const [flag, setFlag] = useState(false)
	useEffect(() => {
		// let time = setTimeout(() => {
		// 	setFlag(true)
		// }, 1000)
		// return () => {
		// 	clearTimeout(time)
		// }
		setFlag(true)
	}, [ref])
	return flag
}

export default useCategoryFlag
