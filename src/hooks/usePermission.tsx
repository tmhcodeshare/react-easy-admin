import { useLocation, useNavigate } from "react-router-dom"
import { useAppSelector } from "@/store"
import {
	saveUserInfoAction,
	saveDynamicRouteListAction,
} from "@/store/modules/user"
import { routers } from "@/router"
import { shallowEqual, useDispatch } from "react-redux"
import { useEffect } from "react"
import { reqUserInfo } from "@/api/user"
import { filterRoutes, findRoute, getTimeRegion } from "@/utils"
import { dynamicRoutes } from "@/router/routers"
import { notification } from "antd"
import { RouteObject } from "@/router/type"
const cloneDeep = require("lodash/cloneDeep")
// 静态路由
const constance = ["/login", "/", "/home", "/screen", "/404"].map((path) => {
	return findRoute(path, routers)
})
// 任意路由
const anyroute = ["*"].map((path) => findRoute(path, routers))

const intercept = (
	dynamicRouters: RouteObject[],
	pathname: string,
	navigate: Function,
) => {
	const routerList = [...constance, ...dynamicRouters, ...anyroute]
	const route = findRoute(pathname, routerList)
	if (Object.keys(route).length === 0) {
		console.log(1111)
		navigate("/404", { replace: true })
	} else {
		navigate(pathname === "/" ? "/home" : pathname)
	}
}

export const usePermission = () => {
	const { token, dynamicRouteList } = useAppSelector(
		(state) => ({
			token: state.userStore.token,
			dynamicRouteList: state.userStore.dynamicRouteList,
		}),
		shallowEqual,
	)
	const dispatch = useDispatch()
	const { pathname } = useLocation()
	const navigate = useNavigate()
	useEffect(() => {
		// 判断有无token
		if (!token.trim()) {
			navigate("/login")
		} else {
			// 判断是否去登录页
			pathname === "/login"
				? navigate("/home", { replace: true })
				: dynamicRouteList !== "[]" // 判断是否有动态路由数据（间接判断有无用户信息）
					? intercept(JSON.parse(dynamicRouteList), pathname, navigate)
					: reqUserInfo()
							.then((res) => {
								if (res.code === 200) {
									console.log("userinfo", res.data)
									notification.success({
										message: "登录成功",
										description: (
											<span>
												尊敬的用户：
												{<span style={{ color: "red" }}>{res.data?.name}</span>}
												，欢迎
												{getTimeRegion()}回来！！！
											</span>
										),
										duration: 6,
									})
									dispatch(saveUserInfoAction(res.data))
									const routes = filterRoutes(
										cloneDeep(dynamicRoutes),
										res?.data?.routes as string[],
									)
									// 触发仓库更新
									dispatch(saveDynamicRouteListAction(JSON.stringify(routes)))
									// 直接传routes，是防止dynamicRouteList还未更新完成，导致未知错误
									intercept(routes, pathname, navigate)
								}
							})
							.catch((error) => {
								console.log(error)
							})
		}
	}, [token, pathname])
}
