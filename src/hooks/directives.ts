import store from "@/store"
// 引入默认图片
import defaultImg from "@/assets/images/notData.png"
export const authPermission = (value: string) => {
	const { userStore } = store.getState()
	const { userInfo } = userStore
	if (userInfo) {
		const { buttons } = userInfo
		return {
			display: !buttons.includes(value) ? "none" : "block",
		}
	}
}
export const imgError = (e: any) => {
	const img = e.target as any
	img.src = defaultImg
}
