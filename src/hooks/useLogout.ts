import { useDispatch } from "react-redux"
import { useNavigate } from "react-router-dom"
import { clearAction } from "@/store/modules/user"
import { clearAllTagsAction } from "@/store/modules/menus"
// 引入layoutstore中的action
import { restoreLayoutAction } from "@/store/modules/layout"
import { reqLogOut } from "@/api/user"
const useLogout = () => {
	const dispatch = useDispatch()
	const navigate = useNavigate()
	const logout = () => {
		reqLogOut().then((res) => {
			if (res.code === 200) {
				dispatch(clearAction())
				dispatch(clearAllTagsAction())
				dispatch(restoreLayoutAction())
				navigate("/login", { replace: true })
			}
		})
	}
	return logout
}

export default useLogout
