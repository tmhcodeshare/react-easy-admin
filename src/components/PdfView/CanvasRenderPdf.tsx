import React, { useRef, useEffect, memo } from "react"
interface CanvasRenderPdfProps {
	id: string
	page: any
	pdfContainerClientWidth: number
	renderNext: Function
	count: number
}
// import { nanoid } from "nanoid"
const drawPdfByCanvas = (
	canvas: HTMLCanvasElement,
	page: any,
	id: string,
	pdfContainerClientWidth: number,
) => {
	// canvas rendering by pdf.js
	canvas.id = id
	// var vp = page.getViewport(1) // 版本2.0.943和更早的版本中，它采用“常规”参数，即格式为getViewport(scale, rotate, dontFlip)
	let vp = page.getViewport({ scale: 1 }) // 版本2.1.266和更高版本中，它接受一个对象，即格式化为getViewport({ scale, rotation, dontFlip })
	let ctx = canvas.getContext("2d")
	let dpr = window.devicePixelRatio || 1
	let bsr =
		// @ts-ignore
		ctx.webkitBackingStorePixelRatio ||
		// @ts-ignore
		ctx.mozBackingStorePixelRatio ||
		// @ts-ignore
		ctx.msBackingStorePixelRatio ||
		// @ts-ignore
		ctx.oBackingStorePixelRatio ||
		// @ts-ignore
		ctx.backingStorePixelRatio ||
		1
	let ratio = dpr / bsr
	let viewport = page.getViewport({
		scale: pdfContainerClientWidth / vp.width,
	})
	console.log("page", page)
	console.log("vp", vp)
	console.log("dpr", dpr)
	console.log("bsr", bsr)
	console.log("ratio", ratio)
	console.log("viewport", viewport)
	canvas.width = viewport.width * ratio
	canvas.height = viewport.height * ratio

	canvas.style.width = viewport.width + "px"

	canvas.style.height = viewport.height + "px"
	// @ts-ignore x轴y轴分别缩放ratio倍数
	ctx.setTransform(ratio, 0, 0, ratio, 0, 0)
	let renderContext = {
		canvasContext: ctx,
		viewport: viewport,
	}
	console.log("------")
	if (canvas) {
		page.render(renderContext)
	}
}
const CanvasRenderPdf: React.FC<CanvasRenderPdfProps> = ({
	id,
	page,
	pdfContainerClientWidth,
	renderNext,
	count,
}) => {
	const canvasRef = useRef<HTMLCanvasElement>(null)
	useEffect(() => {
		// 通过IntersectionObserver API 实现多页pdf懒加载渲染，减轻浏览器渲染压力
		// 创建一个 IntersectionObserver 实例
		const observer = new IntersectionObserver((entries) => {
			entries.forEach((entry) => {
				// 判断是否有交叉
				if (entry.isIntersecting) {
					console.log("元素进入了视口")
					// 触发下一页pdf渲染
					renderNext(count)
					// 结束检查
					if (canvasRef.current) {
						// 结束观察
						observer.unobserve(canvasRef.current)
					}
				}
			})
		})
		if (canvasRef.current !== null) {
			console.log("开始使用canvas渲染pdf")
			drawPdfByCanvas(canvasRef.current, page, id, pdfContainerClientWidth)
			observer.observe(canvasRef.current)
		}
	}, [canvasRef.current])

	return <canvas className="pdf-canvas-item" ref={canvasRef}></canvas>
}

export default memo(CanvasRenderPdf)
