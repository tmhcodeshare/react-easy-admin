import React, { useEffect, useState, useRef, useCallback } from "react"
import CanvasRenderPdf from "./CanvasRenderPdf"
import type { ReactNode } from "react"
import "./index.less"
interface Iprops {
	children?: ReactNode
	link: string
}
interface CanvasItem {
	id: string
	count: number
	page: any
	pdfContainerClientWidth: number
}
// @ts-ignore
import * as PDFJS from "pdfjs-dist"
console.log("PDFJS", PDFJS)
// @ts-ignore
import "pdfjs-dist/build/pdf.worker.entry"
const PdfView: React.FC<Iprops> = (props) => {
	const { link } = props
	// const [url, setUrl] = useState<string>("")
	const cancheRef = useRef()
	const pdfContainerRef = useRef<HTMLDivElement>(null)
	// pdf文件
	const [pdfDoc, setPdfDoc] = useState<any>(null)
	// 渲染pdf的总页面页数
	const [count, setCount] = useState<number>(0)
	// 渲染的当前页数
	const [num, setNum] = useState<number>(1)
	// 渲染canvas的集合
	const [canvasList, setCanvasList] = useState<CanvasItem[]>([])
	const getPdfFile = async (url: string) => {
		console.log("getPdfFile", url)
		fetch(url)
			.then((response) => {
				console.log(response)
				return response.blob()
			})
			.then((data) => {
				console.log(data)
				const objectURL = URL.createObjectURL(data)
				console.log(objectURL)
				_loadFile(objectURL)
			})
			.catch((error) => {
				console.error("Error:", error)
			})
	}
	const _loadFile = (url: string) => {
		const loadtask = PDFJS.getDocument(url)
		loadtask.promise
			.then((pdf: any) => {
				console.log("pdf", pdf)
				setPdfDoc(pdf)
				setCount(pdf.numPages)
			})
			.catch((e: any) => console.dir(e))
	}
	const _renderPage = (
		num: number,
		pdfDoc: any,
		pdfContainer: HTMLDivElement,
		count: number,
	) => {
		pdfDoc.getPage(num).then((page: any) => {
			setCanvasList((canvasList) => {
				const newList = [...canvasList]
				// const canvas = createElement("canvas", { id: `pdfCanvas${num}` })
				newList.push({
					id: `pdfCanvas${num}`,
					page,
					pdfContainerClientWidth: pdfContainer.clientWidth,
					count,
				})
				return newList
			})
			console.log("page", page)
			// 继续渲染下一页
			console.log("count", count)
		})
	}

	const renderNext = useCallback(
		(count: number) => {
			console.log("渲染下一页")
			setNum((num) => {
				console.log("num-------------", num)
				console.log("count-------------", count)
				if (count > num) {
					const newNum = num + 1
					console.log("newNum-------------", newNum)
					return newNum
				}
				return num
			})
		},
		[cancheRef],
	)
	// 副作用
	useEffect(() => {
		getPdfFile(link)
	}, [link])
	useEffect(() => {
		if (pdfDoc && pdfContainerRef.current) {
			if (count >= num) {
				_renderPage(num, pdfDoc, pdfContainerRef.current, count)
			}
		}
	}, [pdfContainerRef.current, pdfDoc, count, num])

	return (
		<div ref={pdfContainerRef} id="pdf-canvas-wrap">
			{canvasList.map((item) => {
				return (
					<CanvasRenderPdf key={item.id} renderNext={renderNext} {...item} />
				)
			})}
		</div>
	)
}

export default PdfView
