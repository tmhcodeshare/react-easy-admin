import React, {
	// useEffect,
	useRef,
	useState,
	memo,
	useMemo,
	useCallback,
} from "react"
// import { useNavigate } from "react-router-dom"
// import { useDispatch } from "react-redux"
import type { ReactNode } from "react"
import { Button, message, Slider, Table, Flex } from "antd"
import type { TableProps } from "antd"
import { PlusOutlined } from "@ant-design/icons"
import axios from "axios"
import "./index.less"
// import { setLinkAction } from "@/store/modules/pdfview"
import PdfView from "../PdfView"
// @ts-ignore
import SliceWorker from "@/utils/sliceUpload.worker.js"
// 组件props TsType
interface Iprops {
	children?: ReactNode
	width?: string
	height?: string
	type: string
	threshold?: number
	sliceSize?: number
}
type CustomAbortController = AbortController
// 表格数据项TsType
interface TableItem {
	key: number
	url: string
	name: string
	size: number
	type: string
	link: string
	file: File
	progress: number
	controller: CustomAbortController | null
}
// 表格列TsType
type ColumnsType<T extends object> = TableProps<T>["columns"]
// sizeformat函数
const sizeformat = (size: number) => {
	return (size / 1024 / 1024).toFixed(2) + "M"
}
const Uploader: React.FC<Iprops> = (props) => {
	const {
		width = "100%",
		height = "320px",
		type,
		threshold = 20 * 1024 * 1024,
		sliceSize = 5 * 1024 * 1024,
	} = props
	console.log(sliceSize)
	// const dispatch = useDispatch()
	// const navigate = useNavigate()
	const inputRef = useRef<HTMLInputElement>(null)
	const cacheRef = useRef(null)
	// const [progress, setProgress] = useState<number>(0)
	// 所有上传文件集合
	const [taskList, setTaskList] = useState<Array<TableItem>>([])
	// 表格列
	const columns: ColumnsType<TableItem> = useMemo(() => {
		return [
			{
				title: "文件名",
				dataIndex: "name",
				key: "name",
			},
			{
				title: "文件大小",
				dataIndex: "size",
				key: "size",
				// 类似 format
				render: (size: number) => {
					return sizeformat(size)
				},
			},
			{
				title: "文件类型",
				dataIndex: "type",
				key: "type",
			},
			{
				title: "上传进度",
				dataIndex: "progress",
				key: "progress",
				render: (progress: number) => {
					return (
						<div
							style={{
								display: "flex",
								justifyContent: "space-between",
								alignItems: "center",
							}}
						>
							<Slider
								style={{ flex: 1 }}
								className="slider"
								step={0.5}
								tooltip={{ formatter: null }}
								value={progress}
							/>
							<span style={{ marginLeft: "2px" }}>{progress}%</span>
						</div>
					)
				},
			},
			{
				title: "操作",
				dataIndex: "action",
				key: "action",
				// @ts-ignore
				render: (_, record: TableItem) => {
					return (
						<Flex gap="small">
							<Button
								type="primary"
								disabled={[0, 100].includes(record.progress)}
								onClick={() => {
									const controller = record.controller as CustomAbortController
									controller.abort()
									simplifySetTaskList<"progress">("progress", 0, record.key)
								}}
							>
								取消上传
							</Button>
							<Button
								disabled={record.progress !== 0}
								onClick={() => upload(record)}
							>
								上传
							</Button>
							<Button
								disabled={record.progress !== 100}
								onClick={() => preview(record.file)}
								type="primary"
							>
								查看
							</Button>
							<Button
								onClick={() => deleteHandle(record.key)}
								danger
								type="primary"
							>
								删除
							</Button>
							<Button
								disabled={record.progress !== 100}
								onClick={() => newPagePreviwe(record.file)}
								type="primary"
							>
								浏览器预览
							</Button>
						</Flex>
					)
				},
			},
		]
	}, [cacheRef])
	const baseUrl = "http://localhost:6061/api/upload/"
	// const baseUrl = useMemo(() => {
	// return taskList.length > 1
	// 	? "https://file.io/~/"
	// 	: "https://file.io/?title="
	// return "http://localhost:6061/api/upload/"
	// }, [taskList.length])
	// baseUrl变化，立即重新设置task的url
	// useEffect(() => {
	// 	setTaskList((taskList) => {
	// 		const newList = [...taskList]
	// 		newList.forEach((task) => {
	// 			task.url = baseUrl + encodeURIComponent(task.name)
	// 		})
	// 		return newList
	// 	})
	// }, [baseUrl])
	const [count, setCount] = useState<number>(0)
	console.log(count)
	// 当前上传文件列表的长度
	const [uploadcount, setUploadCount] = useState<number>(0)
	const [link, setLink] = useState<string>("")
	// 整理每一项任务
	const getTaskItem = useCallback(
		(file: File, key: number): TableItem => {
			const { name, size, type } = file
			const item = {
				key,
				url: baseUrl + (file.size >= threshold ? "slice" : ""),
				name,
				size,
				type,
				link: "",
				file,
				progress: 0,
				controller: null,
			}
			console.log(name, item)
			return item
		},
		[cacheRef],
	)
	// 收集upload任务
	const arrangeTask = useCallback(
		(payload: FileList) => {
			if (Object.keys(payload).length > 1) {
				Object.keys(payload).forEach((key: string) => {
					const file = (payload as FileList)[Number(key)]
					// 上传文件列表去重
					if (taskList.some((task) => task.name === file.name)) {
						return
					}
					setTaskList((taskList) => {
						const newList = [...taskList, getTaskItem(file, taskList.length)]
						setUploadCount(newList.length)
						return newList
					})
				})
			} else {
				setTaskList((taskList) => {
					const taskItem = getTaskItem(payload[0], taskList.length)
					const newList = [...taskList, taskItem]
					setUploadCount(newList.length)
					return newList
				})
			}
		},
		[cacheRef],
	)
	const handleUpload = (e: any) => {
		// setFlag(true) // 异步更新
		const files = e.target.files ? e.target.files : e.dataTransfer.files
		console.log("files", files)
		/* if (files.length > 0) {
			// 获取第一个选中的文件/文件夹的webkitEntry
			const entry = files[0].webkitGetAsEntry()

			if (entry) {
				// 检查webkitEntry是否为目录
				if (entry.isDirectory) {
					console.log("用户选择了一个文件夹")
				} else {
					console.log("用户选择了一个文件")
				}
			} else {
				console.log("无法获取webkitEntry")
			}
		} */
		/* if (flag) {
			message.error({
				content: "上份传输任务还未完成，请勿重复操作😆😆😆",
			})
			return
		} */
		console.log("files", files)
		if (!preCheck(files)) {
			return
		}
		// 收集文件信息整成上传任务列表
		arrangeTask(files)
	}
	// 上传前所有文件校验
	const preCheck = (files: any) => {
		const keys = Object.keys(files).filter((key) => !isNaN(Number(key)))
		for (let i = 0; i < keys.length; i++) {
			const key = keys[i]
			if (!access(files[key])) {
				message.error({
					content: "仅支持上传pdf文件😆😆😆",
				})
				return false
			}
			// if (files[key].size > 10 * 1024 * 1024) {
			// 	message.error({
			// 		content: "文件大小不能超过10M😆😆😆",
			// 	})
			// 	return false
			// }
		}
		return true
	}
	// 上传文件类型检测
	const access = (file: File) => {
		return file.type.includes(type)
	}
	// 按并发数上传文件
	const uploadByConcurrency = (taskList: Array<TableItem>, maxSize = 6) => {
		const l = Math.min(taskList.length, maxSize)
		// 指针直接指向并发的任务数的最后一个任务下标处
		setCount(l - 1)
		for (let i = 0; i < l; i++) {
			const task = taskList[i]
			upload(task)
		}
	}
	// 上传文件
	function simplifySetTaskList<T extends keyof TableItem>(
		key: T,
		value: TableItem[T],
		index: number,
	) {
		setTaskList((taskList) => {
			const newList = [...taskList]
			newList[index][key] = value
			return newList
		})
	}
	const upload = (task: TableItem) => {
		const formDate = new FormData()
		const { url, file, key: index } = task
		const controller = new AbortController()
		if (file.size >= threshold) {
			const work = new SliceWorker()
			simplifySetTaskList<"controller">("controller", controller, index)
			// controller对象（包括controller.signal对象无法被序列化，所以，无法通过postMessage传递给worker）
			controller.signal.addEventListener("abort", () => {
				work.postMessage({ file, url, sliceSize, type: "abort" })
			})
			work.postMessage({ file, url, sliceSize, type: "start" })
			// @ts-ignore
			work.onmessage = ({ data }) => {
				if (data.precent >= 100) {
					console.log("分片上传进度100%")
					setCount((count) => {
						if (count >= uploadcount - 1) {
							return count
						}
						const newVal = count + 1
						upload(taskList[newVal])
						return newVal
					})
					simplifySetTaskList<"link">("link", data.url, index)
				}
				// console.log(data)
				simplifySetTaskList<"progress">(
					"progress",
					Math.floor(data.precent),
					index,
				)
			}
			return
		}
		formDate.append("file", file)
		/* setTaskList((taskList) => {
			const newList = [...taskList]
			newList[index].cancel = controller.abort
			return newList
		}) */
		simplifySetTaskList<"controller">("controller", controller, index)
		axios({
			url,
			method: "post",
			data: formDate,
			onUploadProgress: (progressEvent: any) => {
				// 计算上传进度
				const percentCompleted = Math.round(
					(progressEvent.loaded * 100) / progressEvent.total,
				)
				simplifySetTaskList<"progress">("progress", percentCompleted, index)
				console.log(`Upload Progress: ${percentCompleted}%`)
			},
			signal: controller.signal,
		})
			.then((response: any) => {
				setCount((count) => {
					if (count >= uploadcount - 1) {
						return count
					}
					const newVal = count + 1
					upload(taskList[newVal])
					return newVal
				})
				simplifySetTaskList<"link">("link", response.data.link, index)
				// 上传完成时处理响应
				// inputRef.current!.value = ""
				console.log(response)
			})
			.catch((error) => {
				console.error("Error:", error)
			})
	}
	// 选择文件点击按钮回调
	const btnHandle = () => {
		inputRef.current?.click()
	}
	// 上传点击按钮回调
	const emitUpload = () => {
		if (taskList.length <= 0) {
			message.error({
				content: "请选择上传pdf文件😆😆😆",
			})
			return
		}
		uploadByConcurrency(taskList.filter((task) => task.progress !== 100))
	}
	// 删除按钮回调
	const deleteHandle = (index: number) => {
		setTaskList((taskList) => {
			const newList = [...taskList]
			newList.splice(index, 1)
			// 每次删除某一项task后，重新设置key
			const newArr = newList.map((task, index) => {
				return {
					...task,
					key: index,
				}
			})
			return newArr
		})
	}
	// 拖拽事件回调
	const handleDrop = (e: any) => {
		e.stopPropagation()
		e.preventDefault()
		handleUpload(e)
	}
	const handleDragover = (e: any) => {
		e.stopPropagation()
		e.preventDefault()
		e.dataTransfer.dropEffect = "copy"
	}
	// 预览
	const preview = (file: File) => {
		const link = URL.createObjectURL(file)
		console.log(link)
		setLink(link)
		/* dispatch(setLinkAction(link))
		navigate("/upload/fileView", { replace: true }) */
	}
	// 浏览器预览
	const newPagePreviwe = (file: File) => {
		const link = URL.createObjectURL(file)
		console.log(link)
		const a = document.createElement("a")
		a.href = link
		a.target = "_blank"
		a.click()
	}
	return (
		<>
			{link !== "" ? (
				<>
					<Button onClick={() => setLink("")} type="link">
						返回
					</Button>
					<PdfView link={link}></PdfView>
				</>
			) : (
				<>
					<div
						className="upload-container"
						style={{ width: width, height: height }}
					>
						<input
							ref={inputRef}
							className="upload-input"
							type="file"
							accept={type}
							onChange={handleUpload}
							multiple
						/>
						<div
							className="drop"
							onDrop={handleDrop}
							onDragOver={handleDragover}
							onDragEnter={handleDragover}
						>
							<PlusOutlined
								style={{ fontSize: "60px", marginBottom: "10px" }}
							/>
							<span>将文件拖到此处,仅支持上传pdf文件😆😆😆</span>
						</div>
						<Button type="primary" onClick={btnHandle}>
							请选择上传文件
						</Button>
					</div>
					<div>
						<Table
							dataSource={taskList}
							columns={columns}
							pagination={{
								pageSize: 10,
								pageSizeOptions: ["10", "20", "30", "40"],
							}}
						/>
						<Button type="primary" onClick={emitUpload}>
							上传文件
						</Button>
					</div>
				</>
			)}
		</>
	)
}
export default memo(Uploader)
