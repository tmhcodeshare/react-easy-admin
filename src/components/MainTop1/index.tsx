import React, { memo } from "react"
import { Button, Form, Input, Card, Space } from "antd"
interface Iprops {
	formName: string
	initialValues: any
	name: string
	label: string
	finshed: any
}
// const test = (prevProps: any, nextProps: any) => {
// 	const keys = Object.keys(prevProps)
// 	return keys.every((key) => Object.is(prevProps[key], nextProps[key]))
// }
const MainTop1: React.FC<Iprops> = (props) => {
	console.log("MainTop1渲染了")

	const { formName, initialValues, name, label, finshed } = props
	// 搜索框相关回调
	const [form] = Form.useForm()
	const onFinish = (values: any) => {
		finshed(values)
		form.setFieldsValue({ [name]: "" })
	}
	const resetHandle = () => {
		finshed({ [name]: "" })
		form.setFieldsValue({ [name]: "" })
	}
	return (
		<Card>
			<Form
				form={form}
				name={formName}
				layout="inline"
				initialValues={initialValues}
				onFinish={onFinish}
				style={{ justifyContent: "space-between" }}
			>
				<Form.Item name={name} label={label}>
					<Input placeholder="请输入名称" />
				</Form.Item>
				<Form.Item>
					<Space>
						<Button type="primary" htmlType="submit">
							搜索
						</Button>
						<Button onClick={resetHandle}>重置</Button>
					</Space>
				</Form.Item>
			</Form>
		</Card>
	)
}

export default memo(MainTop1)
