import React, { useEffect } from "react"
import { Form, Select, Card } from "antd"
import {
	fetchC1Options,
	fetchC2Options,
	fetchC3Options,
	category1IdAction,
	category2IdAction,
	category3IdAction,
	c2ListAction,
	c3ListAction,
	clearAction,
} from "@/store/modules/category"
import "./index.less"
import { shallowEqual, useDispatch } from "react-redux"
import { useAppSelector } from "@/store"
interface Iprops {
	isDisabled: number
	flag: boolean
}
const MainTop2: React.FC<Iprops> = (props) => {
	console.log("MainTop2渲染了")

	const { isDisabled, flag } = props
	const { c1List, c2List, c3List, category1Id, category2Id, category3Id } =
		useAppSelector(
			(state) => ({
				c1List: state.categorySore.c1List,
				c2List: state.categorySore.c2List,
				c3List: state.categorySore.c3List,
				category1Id: state.categorySore.category1Id,
				category2Id: state.categorySore.category2Id,
				category3Id: state.categorySore.category3Id,
			}),
			shallowEqual,
		)
	const dispatch = useDispatch()
	useEffect(() => {
		// console.log(1111)

		dispatch(fetchC1Options() as any)
		return () => {
			// console.log(2222)

			dispatch(clearAction())
		}
	}, [dispatch])
	useEffect(() => {
		if (flag && category1Id) {
			dispatch(c2ListAction([]))
			dispatch(c3ListAction([]))
			dispatch(category2IdAction(""))
			dispatch(category3IdAction(""))
			dispatch(fetchC2Options(category1Id as number) as any)
		}
	}, [category1Id])
	useEffect(() => {
		if (flag && category2Id) {
			dispatch(c3ListAction([]))
			dispatch(category3IdAction(""))
			dispatch(fetchC3Options(category2Id as number) as any)
		}
	}, [category2Id])
	const handleChange1 = (value: string) => {
		dispatch(category1IdAction(Number(value)))
	}
	const handleChange2 = (value: string) => {
		dispatch(category2IdAction(Number(value)))
	}
	const handleChange3 = (value: string) => {
		dispatch(category3IdAction(Number(value)))
	}
	return (
		<Card className="maintop2">
			<Form layout="inline">
				<Form.Item label="一级分类">
					<Select
						className="input-w"
						placeholder="请选择一级类目"
						disabled={isDisabled !== 0}
						value={`${category1Id}`}
						onChange={handleChange1}
						options={c1List.map((it) => ({
							value: `${it.id}`,
							label: it.name,
						}))}
					/>
				</Form.Item>
				<Form.Item label="二级分类">
					<Select
						className="input-w"
						placeholder="请选择二级类目"
						disabled={isDisabled !== 0}
						value={`${category2Id}`}
						onChange={handleChange2}
						options={c2List.map((it) => ({
							value: `${it.id}`,
							label: it.name,
						}))}
					/>
				</Form.Item>
				<Form.Item label="三级分类">
					<Select
						className="input-w"
						placeholder="请选择三级类目"
						disabled={isDisabled !== 0}
						value={`${category3Id}`}
						onChange={handleChange3}
						options={c3List.map((it) => ({
							value: `${it.id}`,
							label: it.name,
						}))}
					/>
				</Form.Item>
			</Form>
		</Card>
	)
}
export default MainTop2
