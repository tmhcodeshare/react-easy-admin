import React, { memo } from "react"
// import Icon from "@ant-design/icons"
import { ReactSVG } from "react-svg"
interface Iprops {
	size: string
	svgName: string
}
const SvgIcon: React.FC<Iprops> = ({ size, svgName }) => {
	console.log(svgName)
	return (
		<ReactSVG
			afterInjection={(svg) => {
				// console.log("svgdom", svg)
				svg.setAttribute("style", "width: 100%; height: 100%;")
			}}
			style={{ width: size }}
			src={svgName}
		/>
	)
}

export default memo(SvgIcon)
