import React, { ReactNode } from "react"
interface Iprops {
	children: ReactNode
}
interface Istate {
	hasError: boolean
}
class ErrorBoundary extends React.Component<Iprops, Istate> {
	constructor(props: Iprops) {
		super(props)
		this.state = { hasError: false }
	}
	static getDerivedStateFromError(error: any) {
		console.log(error)

		return { hasError: true }
	}
	componentDidCatch(error: Error, errorInfo: React.ErrorInfo): void {
		console.log(error, errorInfo)
	}
	render() {
		return this.state.hasError ? (
			<h1>Something went wrong.</h1>
		) : (
			this.props.children
		)
	}
}
export default ErrorBoundary
