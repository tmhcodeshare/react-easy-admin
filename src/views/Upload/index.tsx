import React, { memo } from "react"
import UpLoader from "@/components/UpLoader"
import KeepAlive from "react-activation"
const Upload: React.FC = () => {
	return (
		<KeepAlive>
			<UpLoader type="pdf" />
		</KeepAlive>
	)
}

export default memo(Upload)
