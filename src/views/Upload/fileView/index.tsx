import React, { memo } from "react"
import { useAppSelector } from "@/store"
import PdfView from "@/components/PdfView"
import { shallowEqual } from "react-redux"

const Upload: React.FC = () => {
	const { link } = useAppSelector((state) => {
		return {
			link: state.pdfviewStore.link,
		}
	}, shallowEqual)
	return <>{link !== "" ? <PdfView link={link} /> : "暂无显示内容"}</>
}

export default memo(Upload)
