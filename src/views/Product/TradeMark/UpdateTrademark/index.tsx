import React, { useEffect, useState } from "react"
import { Modal, Form, Space, Button, Input } from "antd"
import UploadAvatar from "./UploadAvatar"
import { useForm } from "antd/es/form/Form"
import type { TableItem } from "@/views/Product/TradeMark"
interface Iprops {
	close: any
	initalData: TableItem
	open: boolean
	finished: any
}
const formItemLayout = {
	labelCol: { span: 5 },
	wrapperCol: { span: 15 },
}
const UpdateTrademark: React.FC<Iprops> = (props) => {
	const { initalData, open, close, finished } = props
	// hooks
	const [imageUrl, setImageUrl] = useState(initalData.logoUrl)
	const [form] = useForm()
	useEffect(() => {
		form.setFieldsValue(initalData)
	}, [initalData])
	// callback
	const onFinish = (values: any) => {
		console.log("Received values of form: ", values)
		const data = Object.assign(initalData, values, { logoUrl: imageUrl })
		delete data.order
		delete data.open
		finished(data)
		close()
	}
	return (
		<Modal
			style={{ padding: "20px" }}
			footer={null}
			open={open}
			onCancel={close}
		>
			<Form onFinish={onFinish} {...formItemLayout} form={form}>
				<Form.Item label="品牌名称" name="tmName">
					<Input />
				</Form.Item>
				<Form.Item label="品牌LOGO" name="logoUrl">
					<UploadAvatar
						imageUrl={imageUrl}
						finished={(url: string) => setImageUrl(url)}
					/>
				</Form.Item>
				<Form.Item style={{ textAlign: "center" }}>
					<Space>
						<Button onClick={close}>取消</Button>
						<Button type="primary" htmlType="submit">
							确认
						</Button>
					</Space>
				</Form.Item>
			</Form>
		</Modal>
	)
}

export default UpdateTrademark
