import React, { memo, useEffect, useState } from "react"
import { Button, Table, Popconfirm } from "antd"
import { PlusOutlined, EditOutlined, DeleteOutlined } from "@ant-design/icons"
import { authPermission, imgError } from "@/hooks/directives"
import {
	reqTradeMarkList,
	reqAddOrUpdateTrademark,
	reqDelateTradeMark,
} from "@/api/productions/trademark"
import type { TradeMarkDate } from "@/api/productions/trademark/type"
import UpdateTrademark from "./UpdateTrademark"
export interface TableItem extends TradeMarkDate {
	order?: number
	open?: boolean
}
const initalRowData: TableItem = {
	order: -1,
	id: undefined,
	createTime: "",
	logoUrl: "",
	tmName: "",
	updateTime: "",
}
const { Column } = Table
const TradeMark: React.FC = () => {
	// 封装接口
	const getTradeMarkList = (page: number, limit: number) => {
		reqTradeMarkList(page, limit).then((res) => {
			if (res.code === 200) {
				setTableData(res.data.records.map((it, i) => ({ ...it, order: i + 1 })))
				setTotal(res.data.total)
			}
		})
	}
	const removeTrademark = (id: number) => {
		reqDelateTradeMark(id).then((res) => {
			if (res.code === 200) {
				tableData.length <= 1 && page * pageSize === total
					? setPage(page - 1)
					: getTradeMarkList(page, pageSize)
			}
		})
	}
	const addorUpdataTrademark = (values: TableItem) => {
		reqAddOrUpdateTrademark(values).then((res) => {
			if (res.code === 200) {
				getTradeMarkList(page, pageSize)
			}
		})
	}
	// table hooks
	const [tableData, setTableData] = useState<TableItem[]>([])
	const [rowData, setRowData] = useState<TableItem>(initalRowData)
	// pagination hooks
	const [page, setPage] = useState(1)
	const [pageSize, setPageSize] = useState(3)
	const [total, setTotal] = useState(0)
	// Modal hooks
	const [isModalOpen, setIsModalOpen] = useState(false)
	// Modal callback
	const openHandle = (record: TableItem = initalRowData) => {
		setRowData(record)
		setIsModalOpen(true)
	}
	const closeHandle = () => {
		setRowData(initalRowData)
		setIsModalOpen(false)
	}
	useEffect(() => {
		getTradeMarkList(page, pageSize)
	}, [page, pageSize])
	return (
		<>
			<Button
				type="primary"
				style={authPermission("btn.Trademark.add")}
				icon={<PlusOutlined />}
				onClick={() => openHandle()}
			>
				添加品牌
			</Button>
			<Table
				dataSource={tableData}
				style={{ marginTop: "20px" }}
				rowKey="id"
				bordered
				pagination={{
					showQuickJumper: true,
					showSizeChanger: true,
					current: page,
					pageSize: pageSize,
					total: total,
					pageSizeOptions: [3, 5, 7, 9],
					onChange(page, pageSize) {
						setTableData([])
						setPage(page)
						setPageSize(pageSize)
					},
					showTotal(total) {
						return `Total ${total} items`
					},
				}}
			>
				<Column title="序号" dataIndex="order" key="order" align="center" />
				<Column
					title="品牌名称"
					dataIndex="tmName"
					key="tmName"
					align="center"
					render={(_, record: any) => (
						<pre style={{ color: "chocolate" }}>{record?.tmName}</pre>
					)}
				/>
				<Column
					title="品牌LOGO"
					dataIndex="logoUrl"
					key="logoUrl"
					align="center"
					render={(_, record: TableItem) => (
						<img
							onError={imgError}
							src={record.logoUrl}
							style={{ width: "100px", height: "100px" }}
						/>
					)}
				/>
				<Column
					title="操作"
					dataIndex="action"
					key="action"
					align="center"
					render={(_, record: TableItem) => (
						<div
							style={{
								display: "flex",
								gap: "8px",
								justifyContent: "center",
							}}
						>
							<Button
								type="primary"
								style={authPermission("btn.Trademark.update")}
								icon={<EditOutlined />}
								onClick={() => openHandle(record)}
							>
								编辑
							</Button>
							<Popconfirm
								title="温馨提示！！！"
								description="确认要删除该项吗？"
								open={record?.open}
								onConfirm={() => removeTrademark(record.id as number)}
							>
								<Button
									type="primary"
									style={authPermission("btn.Trademark.remove")}
									icon={<DeleteOutlined />}
									danger
								>
									删除SPU
								</Button>
							</Popconfirm>
						</div>
					)}
				/>
			</Table>
			{isModalOpen && (
				<UpdateTrademark
					initalData={rowData}
					open={isModalOpen}
					close={closeHandle}
					finished={(values: TableItem) => addorUpdataTrademark(values)}
				/>
			)}
		</>
	)
}
export default memo(TradeMark)
