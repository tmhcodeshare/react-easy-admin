import React, { memo, useEffect, useState } from "react"
import { Table, Button, Popconfirm } from "antd"
import {
	ArrowDownOutlined,
	ArrowUpOutlined,
	EditOutlined,
	EyeOutlined,
	DeleteOutlined,
} from "@ant-design/icons"
import { nanoid } from "nanoid"
import ShowDetail from "./ShowDetail"
// 引入接口
import {
	reqSkuList,
	reqOnSale,
	reqCancelSale,
	reqDeleteSku,
} from "@/api/productions/sku"
// 引入ts类型
import type { SKU } from "@/api/productions/sku/type"
import { authPermission, imgError } from "@/hooks/directives"
import "./index.less"
const { Column } = Table
const Sku: React.FC = () => {
	// upper Api
	const getSkuList = (page: number, pageSize: number) => {
		setLoading(true)
		reqSkuList(page, pageSize)
			.then((res) => {
				if (res.code === 200) {
					setTotal(res.data.total)
					setSkuList(res.data.records)
				}
			})
			.catch((err) => console.log(err))
			.finally(() => setLoading(false))
	}
	const onSale = (record: any) => {
		reqOnSale(record.id)
			.then((res) => {
				if (res.code === 200) {
					getSkuList(page, limit)
				}
			})
			.catch((err) => console.log(err))
	}
	const cancelSale = (record: any) => {
		reqCancelSale(record.id)
			.then((res) => {
				if (res.code === 200) {
					getSkuList(page, limit)
				}
			})
			.catch((err) => console.log(err))
	}
	const removeSku = (id: number) => {
		reqDeleteSku(id)
			.then((res) => {
				if (res.code === 200) {
					skuList.length <= 1 && page * limit === total
						? setPage(page - 1)
						: getSkuList(page, limit)
				}
			})
			.catch((err) => console.log(err))
	}
	// table hooks
	const [skuList, setSkuList] = useState<SKU[]>([])
	const [loading, setLoading] = useState(false)
	// pagination hooks
	const [total, setTotal] = useState(0)
	const [page, setPage] = useState(1)
	const [limit, setLimit] = useState(10)
	// ShowDetail hooks
	const [show, setShow] = useState(false)
	const [skuId, setSkuId] = useState<number>(-1)
	useEffect(() => {
		getSkuList(page, limit)
	}, [page, limit])
	// ShowDetail callback
	const open = (record: any) => {
		setShow(true)
		setSkuId(record.id)
	}
	const close = () => {
		setShow(false)
	}
	// Popconfirm callback
	const confirmHandleOk = (record: any) => {
		removeSku(record.id)
	}
	return (
		<>
			<Table
				bordered
				loading={loading}
				dataSource={skuList.map((it, index) => ({
					...it,
					order: index + 1,
					key: nanoid(),
				}))}
				pagination={{
					showQuickJumper: true,
					showSizeChanger: true,
					total: total,
					position: ["bottomRight"],
					pageSize: limit,
					current: page,
					pageSizeOptions: [5, 10, 15, 20],
					onChange(page, pageSize) {
						setSkuList([])
						setPage(page)
						setLimit(pageSize)
					},
					showTotal(total) {
						return `Total ${total} items`
					},
				}}
			>
				<Column title="序号" dataIndex="order" key="order" align="center" />
				<Column
					ellipsis={{ showTitle: true }}
					title="名称"
					dataIndex="skuName"
					key="skuName"
					align="center"
				/>
				<Column
					ellipsis={{ showTitle: true }}
					title="描述"
					dataIndex="skuDesc"
					key="skuDesc"
					align="center"
				/>
				<Column
					title="图片"
					dataIndex="skuDefaultImg"
					key="skuDefaultImg"
					align="center"
					render={(_, record: any) => (
						<img
							src={record.skuDefaultImg}
							style={{ width: "150px", height: "150px" }}
							onError={imgError}
						></img>
					)}
				/>
				<Column
					ellipsis={{ showTitle: true }}
					title="重量"
					dataIndex="weight"
					key="weight"
					align="center"
				/>
				<Column
					ellipsis={{ showTitle: true }}
					title="价格"
					dataIndex="price"
					key="price"
					align="center"
				/>
				<Column
					title="操作"
					dataIndex="action"
					key="action"
					width={400}
					align="center"
					fixed="right"
					render={(_, record: any) => (
						<div
							style={{
								display: "flex",
								justifyContent: "center",
								gap: "8px",
							}}
						>
							{record.isSale ? (
								<Button
									className="up"
									type="primary"
									icon={<ArrowDownOutlined />}
									style={{
										backgroundColor: "#909399",
										...authPermission("btn.Sku.updown"),
									}}
									onClick={() => cancelSale(record)}
								></Button>
							) : (
								<Button
									className="up"
									type="primary"
									icon={<ArrowUpOutlined />}
									style={{
										backgroundColor: "#67C23A",
										...authPermission("btn.Sku.updown"),
									}}
									onClick={() => onSale(record)}
								></Button>
							)}

							<Button
								type="primary"
								style={authPermission("btn.Sku.update")}
								icon={<EditOutlined />}
							></Button>
							<Button
								className="up"
								type="primary"
								icon={<EyeOutlined />}
								style={{
									backgroundColor: "#909399",
									...authPermission("btn.Sku.detail"),
								}}
								onClick={() => open(record)}
							></Button>
							<Popconfirm
								title="温馨提示！！！"
								description="确认要删除该项吗？"
								open={record?.open}
								onConfirm={() => confirmHandleOk(record)}
							>
								<Button
									type="primary"
									style={authPermission("btn.Sku.remove")}
									icon={<DeleteOutlined />}
									danger
								></Button>
							</Popconfirm>
						</div>
					)}
				/>
			</Table>
			{show && <ShowDetail skuId={skuId} open={show} close={close} />}
		</>
	)
}
export default memo(Sku)
