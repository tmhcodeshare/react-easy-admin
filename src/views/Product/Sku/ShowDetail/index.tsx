import React, { useEffect, useState } from "react"
import { Skeleton, Row, Col, Drawer, Tag, Carousel } from "antd"
// 引入接口
import { reqSkuDeatil } from "@/api/productions/sku"
// 引入类型
import type { SKU } from "@/api/productions/sku/type"
import "./index.less"
interface Iporps {
	open: boolean
	skuId: number
	close: any
}
const initalSkuData: SKU = {
	category3Id: undefined, // 三级分类id
	spuId: undefined, // 已有SPUid
	tmId: undefined, // 品牌id
	price: undefined, // 价格
	weight: "", // 重量
	skuDesc: "", // sku描述
	skuDefaultImg: "", // sku默认照片
	skuName: "", // sku名称
}
const ShowDetail: React.FC<Iporps> = (props) => {
	// 封装接口
	const getSkuDeatil = (id: number) => {
		setLoading(true)
		reqSkuDeatil(id)
			.then((res) => {
				if (res.code === 200) {
					setSkuData(res.data)
				}
			})
			.catch((err) => console.log(err))
			.finally(() => setLoading(false))
	}
	const { open, close, skuId } = props
	const [loading, setLoading] = useState(true)
	const [skuData, setSkuData] = useState<SKU>(initalSkuData)
	useEffect(() => {
		if (skuId >= 0) {
			getSkuDeatil(skuId)
		}
	}, [])
	const closeHandle = () => {
		setSkuData(initalSkuData)
		close()
	}
	return (
		<Drawer title="查看商品详情" open={open} onClose={closeHandle} width={500}>
			{loading && (
				<div>
					<Row className="gap">
						<Col span={6}>名称</Col>
						<Col span={18}>
							<Skeleton.Input active />
						</Col>
					</Row>
					<Row className="gap">
						<Col span={6}>描述</Col>
						<Col span={18}>
							<Skeleton.Input active />
						</Col>
					</Row>
					<Row className="gap">
						<Col span={6}>价格</Col>
						<Col span={18}>
							<Skeleton.Input active />
						</Col>
					</Row>
					<Row className="gap">
						<Col span={6}>平台属性</Col>
						<Col span={18}>
							<Skeleton.Input active />
						</Col>
					</Row>
					<Row className="gap">
						<Col span={6}>销售属性</Col>
						<Col span={18}>
							<Skeleton.Input active />
						</Col>
					</Row>
					<Row className="gap">
						<Col span={6}>商品图片</Col>
						<Col span={18}>
							<Skeleton.Image
								active
								style={{ width: "250px", height: "250px" }}
							/>
						</Col>
					</Row>
				</div>
			)}
			{!loading && (
				<div>
					<Row className="gap">
						<Col span={6}>名称</Col>
						<Col span={18}>{skuData.skuName}</Col>
					</Row>
					<Row className="gap">
						<Col span={6}>描述</Col>
						<Col span={18}>{skuData.skuDesc}</Col>
					</Row>
					<Row className="gap">
						<Col span={6}>价格</Col>
						<Col span={18}>{skuData.price}</Col>
					</Row>
					<Row className="gap">
						<Col span={6}>平台属性</Col>
						<Col span={18}>
							{skuData.skuAttrValueList &&
								skuData.skuAttrValueList?.map((item, index) => (
									<Tag
										style={{ margin: "5px 5px" }}
										key={index}
									>{`${item.attrName}：${item.valueName}`}</Tag>
								))}
						</Col>
					</Row>
					<Row className="gap">
						<Col span={6}>销售属性</Col>
						<Col span={18}>
							{skuData.skuSaleAttrValueList &&
								skuData.skuSaleAttrValueList?.map((item, index) => (
									<Tag
										style={{ margin: "5px 5px" }}
										key={index}
									>{`${item.saleAttrName}：${item.saleAttrValueName}`}</Tag>
								))}
						</Col>
					</Row>
					<Row className="gap">
						<Col span={6}>商品图片</Col>
						<Col span={18}>
							<Carousel className="carousel" autoplay>
								{skuData.skuImageList &&
									skuData.skuImageList?.map((item, index) => (
										<img
											key={index}
											src={item.imgUrl}
											style={{ width: "100%", height: "100%" }}
											alt=""
										/>
									))}
							</Carousel>
						</Col>
					</Row>
				</div>
			)}
		</Drawer>
	)
}

export default ShowDetail
