import React, { memo, useEffect, useState } from "react"
import { Card, Button, Table, Popconfirm, Space, Tag } from "antd"
import { PlusOutlined, EditOutlined, DeleteOutlined } from "@ant-design/icons"
import { useAppSelector } from "@/store"
import { shallowEqual } from "react-redux"
import { nanoid } from "nanoid"
import {
	reqAttr,
	reqDeleteAttr,
	reqAddOrEditAttr,
} from "@/api/productions/attr"
import type { Attr } from "@/api/productions/attr/type"
import { authPermission } from "@/hooks/directives"
import useCategoryFlag from "@/hooks/useCategoryFlag"
import MainTop2 from "@/components/MainTop2"
import UpdateAttr from "./UpdateAttr"
interface UpdateParams extends Attr {
	key?: string
	order?: number
}
const cloneDeep = require("lodash/cloneDeep")
const { Column } = Table
const initalRowData = {
	attrName: "",
	attrValueList: [],
	categoryId: undefined,
	categoryLevel: 3,
}
const AttrComponent: React.FC = () => {
	// 封装接口
	const getAttr = (
		category1Id: number | string,
		category2Id: number | string,
		category3Id: number | string,
	) => {
		setLoading(true)
		reqAttr(category1Id, category2Id, category3Id)
			.then((res) => {
				if (res.code === 200) {
					setTableData(res.data)
				}
			})
			.catch((error) => console.log(error))
			.finally(() => setLoading(false))
	}
	const { category1Id, category2Id, category3Id } = useAppSelector(
		(state) => ({
			category1Id: state.categorySore.category1Id,
			category2Id: state.categorySore.category2Id,
			category3Id: state.categorySore.category3Id,
		}),
		shallowEqual,
	)
	const [open, setOpen] = useState<number>(0)
	// table hooks
	const [tableData, setTableData] = useState<Attr[]>([])
	const [loading, setLoading] = useState<boolean>(false)
	const [rowData, setRowData] = useState<Attr>(initalRowData)
	// MainTop2组件是否更新状态 hook
	const flag = useCategoryFlag()
	useEffect(() => {
		if (flag && category3Id !== "") {
			console.log(category3Id)
			getAttr(category1Id, category2Id, category3Id)
		}
	}, [flag, category3Id])
	// Popconfirm callback
	const confirmHandleOk = (record: any) => {
		reqDeleteAttr(record.id)
			.then((res) => {
				if (res.code === 200) {
					getAttr(category1Id, category2Id, category3Id)
				} else {
					alert(res.message)
				}
			})
			.catch((err) => console.log(err))
	}
	// updateComponent callback
	const openHandle = (record: any) => {
		setOpen(1)
		if (record.id) {
			setRowData(record)
		}
	}
	const closeHandle = () => {
		setOpen(0)
		setRowData(initalRowData)
	}
	const updateHandle = (val: any) => {
		val.categoryId = category3Id
		const params: UpdateParams = cloneDeep(val)
		if (Object.prototype.hasOwnProperty.call(params, "key")) {
			delete params[`key`]
		}
		if (Object.prototype.hasOwnProperty.call(params, "order")) {
			delete params[`order`]
		}
		reqAddOrEditAttr(params)
			.then((res) => {
				if (res.code === 200) {
					getAttr(category1Id, category2Id, category3Id)
				} else {
					alert(res.message)
				}
			})
			.catch((err) => console.log(err))
	}
	return (
		<>
			<MainTop2 flag={flag} isDisabled={open}></MainTop2>
			{open === 0 && (
				<Card style={{ marginTop: "20px" }}>
					<Button
						type="primary"
						icon={<PlusOutlined />}
						disabled={category3Id === ""}
						style={authPermission("btn.Attr.add")}
						onClick={openHandle}
					>
						添加商品属性
					</Button>
					<Table
						style={{ marginTop: "20px" }}
						loading={loading}
						bordered
						dataSource={tableData.map((it, index) => ({
							...it,
							order: index + 1,
							key: nanoid(),
						}))}
						pagination={false}
					>
						<Column title="#" dataIndex="order" key="order" align="center" />
						<Column
							title="属性名称"
							dataIndex="attrName"
							key="attrName"
							align="center"
						/>
						<Column
							title="属性值名称"
							dataIndex="attrValueList"
							key="attrValueList"
							align="center"
							render={(_, record: any) => (
								<Space>
									{record.attrValueList
										.map((it: any) => {
											if (!it.valueName) {
												return
											}
											return <Tag key={it.id}>{it.valueName}</Tag>
										})
										.filter(Boolean)}
								</Space>
							)}
						/>
						<Column
							title="操作"
							key="action"
							align="center"
							render={(_, record: any) => (
								<div
									style={{
										display: "flex",
										gap: "8px",
										justifyContent: "center",
									}}
								>
									<Button
										style={authPermission("btn.Role.update")}
										icon={<EditOutlined />}
										type="primary"
										onClick={() => openHandle(record)}
									>
										编辑
									</Button>
									<Popconfirm
										title="温馨提示！！！"
										description="确认要删除该项吗？"
										open={record?.open}
										onConfirm={() => confirmHandleOk(record)}
									>
										<Button
											style={authPermission("btn.Role.remove")}
											icon={<DeleteOutlined />}
											type="primary"
											danger
										>
											删除
										</Button>
									</Popconfirm>
								</div>
							)}
						/>
					</Table>
				</Card>
			)}
			{open === 1 && (
				<UpdateAttr
					initialValues={rowData}
					close={closeHandle}
					finished={(val: any) => updateHandle(val)}
				/>
			)}
		</>
	)
}
export default memo(AttrComponent)
