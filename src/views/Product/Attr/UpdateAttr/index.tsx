import React, { useState, useRef, useEffect } from "react"
import { Input, Button, Card, Table, Popconfirm } from "antd"
import {
	PlusOutlined,
	DeleteOutlined,
	RollbackOutlined,
} from "@ant-design/icons"
import type { Attr, AttrValueObj } from "@/api/productions/attr/type"
import { nanoid } from "nanoid"
const cloneDeep = require("lodash/cloneDeep")
interface ListItem extends AttrValueObj {
	order?: number
	key?: string
	preview?: boolean
}
interface Iprops {
	initialValues: Attr
	close: any
	finished: any
}
// 定义输入引用的类型
// type InputRef = React.RefObject<HTMLInputElement>
const { Column } = Table
// 不使用form组件管理状态的时候，内部useState接收传入的Props的值initialValues，为了保证不打破单向数据流
const UpdateAttr: React.FC<Iprops> = (props) => {
	const { initialValues, close, finished } = props
	// props map hooks
	const [keywords, setKeywords] = useState<string>(initialValues["attrName"])
	// table hooks
	const inputRefs = useRef<any[]>([])
	const [index, setIndex] = useState<number>(-1)
	const [lastFocus, setLastFocus] = useState<boolean>(false)
	const [tableData, setTableData] = useState(
		initialValues.attrValueList.map((it, index) => ({
			...it,
			order: index + 1,
			key: nanoid(),
			preview: true,
		})),
	)
	useEffect(() => {
		if (index >= 0 && inputRefs.current[index]) {
			inputRefs.current[index].focus()
		}
	}, [index])
	// useEffect(() => {
	// 	setKey(nanoid())
	// }, [tableData])
	useEffect(() => {
		if (lastFocus) {
			console.log(inputRefs.current)
			inputRefs.current[inputRefs.current.length - 1].focus()
			setLastFocus(false)
		}
	}, [lastFocus])
	// add button callbak
	const addAttr = () => {
		setTableData((val) => {
			val.push({
				key: nanoid(),
				order: val.length + 1,
				preview: false,
				valueName: "",
			})
			return cloneDeep(val)
		})
		setLastFocus(true)
	}
	// table callbacks
	const toEdit = (record: any) => {
		const index = record.order - 1
		setIndex(index)
		setTableData((val) => {
			val[index].preview = false
			console.log(val)

			return cloneDeep(val)
		})
	}
	const toPreview = (e: any, record: any) => {
		const value = e.target.value
		const index = record.order - 1
		// 判空
		if (!value.trim()) {
			alert("属性不能为空")
			return
		}
		// 判重
		if (
			tableData
				.map((it) => it.valueName)
				.filter((it) => it !== record.valueName)
				.includes(value)
		) {
			alert("属性不能重复")
			return
		}
		setIndex(-1)
		setTableData((val) => {
			val[index].preview = true
			val[index].valueName = value
			return cloneDeep(val) // set类hooks，如果返回值是引用类型，需要改变其引用地址，才能刷新页面
		})
	}
	// delete button callback
	const confirmHandleOk = (recode: any) => {
		setTableData((val) => val.filter((it) => it.valueName !== recode.valueName))
	}
	// save button callback
	const save = () => {
		const list = tableData.map((it) => {
			const item: ListItem = cloneDeep(it)
			delete item[`order`]
			delete item[`key`]
			delete item[`preview`]
			return item
		})
		finished({ ...initialValues, attrName: keywords, attrValueList: list })
		close()
	}
	return (
		<Card style={{ marginTop: "20px" }}>
			<div style={{ width: "260px", marginBottom: "20px" }}>
				<Input
					defaultValue={keywords}
					onBlur={(e) => {
						console.log(e.target.value)
						setKeywords(e.target.value)
					}}
					size="large"
					placeholder="请输入属性名称"
				></Input>
			</div>
			<div style={{ display: "flex", gap: "8px" }}>
				<Button
					type="primary"
					icon={<PlusOutlined />}
					disabled={!keywords}
					onClick={addAttr}
				>
					添加属性值
				</Button>
				<Button icon={<RollbackOutlined />} onClick={close}>
					取消
				</Button>
			</div>
			<Table
				style={{ margin: "20px 0" }}
				bordered
				pagination={false}
				dataSource={tableData}
			>
				<Column title="#" dataIndex="order" key="order" align="center" />
				<Column
					title="属性值"
					dataIndex="valueName"
					key="valueName"
					align="center"
					width="60%"
					render={(_, record: any) => (
						<div>
							{record.preview && (
								<p
									onDoubleClickCapture={() => toEdit(record)}
									style={{
										backgroundImage:
											"linear-gradient(-90deg, #ccfbff, #ef96c5)",
										color: "#fff",
									}}
								>
									{record.valueName}
								</p>
							)}
							{!record.preview && (
								<Input
									ref={(el) => (inputRefs.current[`${record.order - 1}`] = el)}
									defaultValue={record.valueName}
									onBlur={(e) => toPreview(e, record)}
								></Input>
							)}
						</div>
					)}
				/>
				<Column
					title="操作"
					dataIndex="action"
					key="action"
					align="center"
					render={(_, record: any) => (
						<Popconfirm
							title="温馨提示！！！"
							description="确认要删除该项吗？"
							open={record?.open}
							onConfirm={() => confirmHandleOk(record)}
						>
							<Button icon={<DeleteOutlined />} type="primary" danger>
								删除
							</Button>
						</Popconfirm>
					)}
				/>
			</Table>
			<div style={{ display: "flex", gap: "8px" }}>
				<Button
					type="primary"
					icon={<PlusOutlined />}
					disabled={!keywords}
					onClick={save}
				>
					保存
				</Button>
				<Button icon={<RollbackOutlined />} onClick={close}>
					取消
				</Button>
			</div>
		</Card>
	)
}
export default UpdateAttr
