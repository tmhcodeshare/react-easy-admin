import React, { useEffect, useRef, useState } from "react"
import { Form, Button, Space, Input, Skeleton, Select, Table } from "antd"
import type { FormInstance } from "antd/lib/form"
// 引入ts类型
import type { Spu, ImageObj, SaleAttr } from "@/api/productions/spu/type"
import type { Attr } from "@/api/productions/attr/type"
import { SKU } from "@/api/productions/sku/type"
// 引入接口
import { reqAttr } from "@/api/productions/attr"
import { reqSaleAttrList, reqImageList } from "@/api/productions/spu"
import { useAppSelector } from "@/store"
import { shallowEqual } from "react-redux"
import { imgError } from "@/hooks/directives"
import "./index.less"
interface InitalValue extends Spu {
	order?: number
	key?: string
}
// interface TableItem extends ImageObj {
// 	key?: string
// }
interface Iprops {
	close: any
	finished: any
	initalValue: InitalValue
}
const { TextArea } = Input
const { Column } = Table
const fixData = function <T>(value: string, k1: keyof T, k2: keyof T) {
	const values = value.split(":")
	return { [k1]: Number(values[0]), [k2]: Number(values[1]) }
}
const arrange = function <T, K>(
	list: K[],
	key: keyof K,
	k1: keyof T,
	k2: keyof T,
) {
	return list
		.map((it: K) => {
			return it[key] ? fixData(it[key] as string, k1, k2) : undefined
		})
		.filter(Boolean)
}
const initalSkuParams: SKU = {
	// 父组件传递数据
	category3Id: undefined, // 三级分类id
	spuId: undefined, // 已有SPUid
	tmId: undefined, // 品牌id
	// 表单收集数据
	skuName: "", // sku名称
	price: undefined, // 价格
	weight: "", // 重量
	skuDesc: "", // sku描述

	skuDefaultImg: "", // sku默认照片
	skuAttrValueList: [],
	skuSaleAttrValueList: [],
	skuImageList: [],
}
const AddSku: React.FC<Iprops> = (props) => {
	// upper Apies
	const getAttr = (
		category1Id: number,
		category2Id: number,
		category3Id: number,
	) => {
		reqAttr(category1Id, category2Id, category3Id)
			.then((res) => {
				if (res.code === 200) {
					setAttrList(res.data)
				}
			})
			.catch((err) => console.dir(err))
	}
	const getSaleAttrList = (spuId: number) => {
		reqSaleAttrList(spuId)
			.then((res) => {
				if (res.code === 200) {
					setSaleAttrList(res.data)
				}
			})
			.catch((err) => console.dir(err))
	}
	const getAllImageList = (spuId: number) => {
		reqImageList(spuId)
			.then((res) => {
				if (res.code === 200) {
					setImageList(res.data)
					setLoading(false)
				}
			})
			.catch((err) => console.dir(err))
	}
	const { initalValue, close, finished } = props
	const { category1Id, category2Id, category3Id } = useAppSelector(
		(state) => ({
			category1Id: state.categorySore.category1Id,
			category2Id: state.categorySore.category2Id,
			category3Id: state.categorySore.category3Id,
		}),
		shallowEqual,
	)
	// form hooks
	const ref1 = useRef<FormInstance>(null)
	const ref2 = useRef<FormInstance>(null)
	const ref3 = useRef<FormInstance>(null)
	const [attrList, setAttrList] = useState<Attr[]>([])
	const [saleAttrList, setSaleAttrList] = useState<SaleAttr[]>([])
	// table hooks
	const [imageList, setImageList] = useState<ImageObj[]>([])
	const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([])

	// Skeleton hook
	const [loading, setLoading] = useState(true)
	useEffect(() => {
		const { id } = initalValue
		ref1.current && ref1.current.setFieldsValue(initalSkuParams)
		getAttr(category1Id as number, category2Id as number, category3Id as number)
		getSaleAttrList(id as number)
		getAllImageList(id as number)
	}, [initalValue])
	useEffect(() => {
		ref2.current?.setFieldsValue(attrList)
	}, [attrList])
	console.log("attrList", attrList)
	console.log("saleAttrList", saleAttrList)

	// select callback
	const selectChangeHandle = (val: any, i: number, type: string) => {
		console.log(val, i, type)
		type === "attr"
			? setAttrList((value) => {
					value[i]["attrIdAndValueId"] = val
					return value
				})
			: setSaleAttrList((value) => {
					value[i]["saleAttrIdAndSaleAttrValueId"] = val
					return value
				})
	}
	// Table callbacks
	const onSelectChange = (newSelectedRowKeys: React.Key[]) => {
		// console.log("selectedRowKeys changed: ", newSelectedRowKeys)
		setSelectedRowKeys([newSelectedRowKeys[newSelectedRowKeys.length - 1]])
	}
	const rowSelection = {
		selectedRowKeys,
		onChange: onSelectChange,
	}

	// form callback
	const onFinish = (values: any) => {
		const { id, tmId } = initalValue
		console.log("values", values)
		const skuAttrValueList = arrange<{ attrId: number; valueId: number }, Attr>(
			attrList,
			"attrIdAndValueId",
			"attrId",
			"valueId",
		)
		const skuSaleAttrValueList = arrange<
			{ saleAttrId: number; saleAttrValueId: number },
			SaleAttr
		>(
			saleAttrList,
			"saleAttrIdAndSaleAttrValueId",
			"saleAttrId",
			"saleAttrValueId",
		)
		const skuDefaultImg =
			imageList.find((it) => it.id === selectedRowKeys[0])?.imgUrl || ""
		const value = Object.assign(initalSkuParams, values, {
			category3Id,
			spuId: id,
			tmId,
			skuAttrValueList,
			skuSaleAttrValueList,
			skuImageList: imageList,
			skuDefaultImg,
		})
		close()
		finished(value)
	}

	return (
		<>
			<Form
				onFinish={onFinish}
				ref={ref1}
				name="add_sku"
				labelCol={{ flex: "100px" }}
			>
				<Form.Item label="sku名称" name="skuName">
					<Input placeholder="sku名称" />
				</Form.Item>
				<Form.Item label="价格(元)" name="price">
					<Input placeholder="价格(元)" />
				</Form.Item>
				<Form.Item label="重量(克)" name="weight">
					<Input placeholder="重量(克)" />
				</Form.Item>
				<Form.Item label="sku描述" name="skuDesc">
					<TextArea placeholder="请输入描述内容......" rows={5} />
				</Form.Item>
			</Form>
			<Skeleton loading={loading} active paragraph={{ rows: 12 }}>
				<div className="item">
					<div className="label">平台属性：</div>
					<Form className="form" ref={ref2} layout="inline" name="attr_list">
						{attrList.map((it, i) => {
							return (
								<Form.Item
									key={it.id}
									label={it.attrName}
									style={{ marginBottom: "10px" }}
								>
									<Select
										value={it.attrIdAndValueId}
										style={{ width: 150 }}
										onChange={(val: any) => selectChangeHandle(val, i, "attr")}
										options={it.attrValueList.map((item) => ({
											label: item.valueName,
											value: `${item.attrId}:${item.id}`,
										}))}
									/>
								</Form.Item>
							)
						})}
					</Form>
				</div>
				<div className="item">
					<div className="label">销售属性：</div>
					<Form
						className="form"
						ref={ref3}
						layout="inline"
						name="sale_attr_list"
					>
						{saleAttrList.map((it, i) => {
							return (
								<Form.Item
									key={it.id}
									label={it.saleAttrName}
									style={{ marginBottom: "10px" }}
								>
									<Select
										value={it.saleAttrIdAndSaleAttrValueId}
										style={{ width: 150 }}
										onChange={(val: any) =>
											selectChangeHandle(val, i, "saleAttr")
										}
										options={it.spuSaleAttrValueList.map((item) => ({
											label: item.saleAttrValueName,
											value: `${item.baseSaleAttrId}:${item.id}`,
										}))}
									/>
								</Form.Item>
							)
						})}
					</Form>
				</div>
				<div className="item">
					<div className="label">图片名称：</div>
					<Table
						className="form"
						pagination={false}
						bordered
						rowSelection={rowSelection}
						dataSource={imageList}
						rowKey="id"
					>
						<Column
							title="图片"
							dataIndex="imgUrl"
							key="imgUrl"
							render={(_, record: ImageObj) => (
								<img onError={imgError} src={record.imgUrl} className="img" />
							)}
						/>
						<Column title="名称" dataIndex="imgName" key="imgName" />
						<Column
							title="操作"
							dataIndex="action"
							key="action"
							render={(_, record: ImageObj) => (
								<Button
									type="primary"
									onClick={() => setSelectedRowKeys([record.id as number])}
								>
									设为默认
								</Button>
							)}
						/>
					</Table>
				</div>
			</Skeleton>
			<Space>
				<Button type="primary" onClick={ref1.current?.submit}>
					Submit
				</Button>
				<Button type="primary" onClick={close}>
					取消
				</Button>
			</Space>
		</>
	)
}
export default AddSku
