import React, { useEffect, useMemo, useState } from "react"
import {
	Form,
	Button,
	Input,
	Select,
	Modal,
	Upload,
	Space,
	Table,
	Tag,
} from "antd"
import {
	PlusOutlined,
	DeleteOutlined,
	CloseCircleOutlined,
} from "@ant-design/icons"
import type { RcFile, UploadProps } from "antd/es/upload"
import type { UploadFile } from "antd/es/upload/interface"
import { nanoid } from "nanoid"
// 引入接口
import {
	reqAllTradeMarkList,
	reqImageList,
	reqAllBaseSaleAttrList,
	reqSaleAttrList,
} from "@/api/productions/spu"
// 引入ts类型
import type {
	Spu,
	TradeMark,
	BaseAttr,
	SaleAttr,
} from "@/api/productions/spu/type"
import "./index.less"
interface TableItem extends SaleAttr {
	order?: number
	key?: string
	flag?: boolean
}
interface InitalValue extends Spu {
	order?: number
	key?: string
}
interface Iprops {
	close: any
	finished: any
	initalValue: InitalValue
}
const { TextArea } = Input
const getBase64 = (file: RcFile): Promise<string> =>
	new Promise((resolve, reject) => {
		const reader = new FileReader()
		reader.readAsDataURL(file)
		reader.onload = () => resolve(reader.result as string)
		reader.onerror = (error) => reject(error)
	})
const cloneDeep = require("lodash/cloneDeep")
const { Column } = Table
const UpdateSpu: React.FC<Iprops> = (props) => {
	// 封装接口
	// 获取全部品牌列表
	const getAllTradeMarkList = () => {
		reqAllTradeMarkList()
			.then((res) => {
				if (res.code === 200) {
					setTradeMarkList(res.data)
				}
			})
			.catch((err) => console.dir(err))
	}
	// 获取全部品牌列表
	const getAllImageList = (spuId: number) => {
		reqImageList(spuId)
			.then((res) => {
				if (res.code === 200) {
					const fileList = res.data.map((it) => {
						return {
							uid: it.id ? `${it.id}` : "",
							name: it.imgName || "",
							url: it.imgUrl,
							status: (it.imgUrl !== undefined ? "done" : "error") as any,
						}
					})
					setFileList(fileList)
				}
			})
			.catch((err) => console.dir(err))
	}
	// 获取该项目全部销售属性（颜色、版本、尺码）
	const getAllBaseSaleAttrList = () => {
		reqAllBaseSaleAttrList()
			.then((res) => {
				if (res.code === 200) {
					setBaseAttrList(res.data)
				}
			})
			.catch((err) => console.dir(err))
	}
	// 获取SPU销售属性列表
	const getSaleAttrList = (spuId: number) => {
		reqSaleAttrList(spuId)
			.then((res) => {
				if (res.code === 200) {
					setTableData(
						res.data.map((it, i) => ({
							...it,
							order: i + 1,
							key: nanoid(),
							flag: false,
						})),
					)
				}
			})
			.catch((err) => console.dir(err))
	}
	const { initalValue, close, finished } = props
	const { id } = initalValue
	// selectoption hooks
	const [tradeMarkList, setTradeMarkList] = useState<TradeMark[]>([])
	// table hooks
	const [baseAttrList, setBaseAttrList] = useState<BaseAttr[]>([])
	const [tableData, setTableData] = useState<TableItem[]>([])
	const [selectAttrValue, setSelectAttrValue] = useState<string | undefined>(
		undefined,
	)
	const unSelectSaleAttrValueList = useMemo(() => {
		return baseAttrList.filter((it) => {
			return !tableData.some((item) => item.saleAttrName === it.name)
		})
	}, [baseAttrList, tableData])
	// upload hooks
	const [previewOpen, setPreviewOpen] = useState(false)
	const [previewImage, setPreviewImage] = useState("")
	const [previewTitle, setPreviewTitle] = useState("")
	const [fileList, setFileList] = useState<UploadFile[]>([])
	const [form] = Form.useForm()
	useEffect(() => {
		getAllTradeMarkList()
		getAllBaseSaleAttrList()
		if (id !== "") {
			getAllImageList(id as number)
			getSaleAttrList(id as number)
		}
		form.setFieldsValue(cloneDeep(initalValue))
	}, [id])
	// form callbacks
	const onFinish = (values: any) => {
		const { category3Id, id } = initalValue
		// arrange
		const spuImageList = fileList.map((it) => {
			const { name, response } = it
			return {
				imgName: name,
				imgUrl: (response && response?.data) || it.url,
			}
		})
		const spuSaleAttrList = tableData.map((it) => {
			const item = cloneDeep(it)
			delete item["order"]
			delete item["key"]
			delete item["flag"]
			return item
		})
		const params: Spu = {
			...values,
			category3Id,
			id,
			spuImageList,
			spuSaleAttrList,
		}
		finished(params)
		close()
	}

	const onFinishFailed = (errorInfo: any) => {
		console.log("Failed:", errorInfo)
	}
	// upload callbacks
	const uploadHandleCancel = () => setPreviewOpen(false)
	const uploadHandlePreview = async (file: UploadFile) => {
		if (!file.url && !file.preview) {
			file.preview = await getBase64(file.originFileObj as RcFile)
		}
		setPreviewImage(file.url || (file.preview as string))
		setPreviewOpen(true)
		setPreviewTitle(
			file.name || file.url!.substring(file.url!.lastIndexOf("/") + 1),
		)
	}
	const uploadHandleChange: UploadProps["onChange"] = ({
		fileList: newFileList,
	}) => {
		setFileList(newFileList)
	}

	// select callback
	const selectHandleChange = (value: any) => {
		setSelectAttrValue(value)
	}

	// addSpu button callback
	const addAttrValue = () => {
		// 整理数据
		if (selectAttrValue) {
			const [baseSaleAttrId, saleAttrName] = selectAttrValue.split(":")
			const valueObj: TableItem = {
				baseSaleAttrId: JSON.parse(baseSaleAttrId),
				saleAttrName,
				spuSaleAttrValueList: [],
				key: nanoid(),
				order: tableData.length + 1,
				flag: false,
			}
			setTableData((val) => [...val, valueObj])
			setSelectAttrValue(undefined)
		}
	}
	// tag remove callback
	const removeTag = (
		e: React.MouseEvent<HTMLElement>,
		record: TableItem,
		i: number,
	) => {
		e.preventDefault()
		const index = (record.order as number) - 1
		setTableData((data) => {
			data[index].spuSaleAttrValueList.splice(i, 1)
			return cloneDeep(data)
		})
	}
	// preview callback
	const toPreview = (e: any, record: TableItem) => {
		const saleAttrValueName = e.target.value || ""
		// 判空
		if (saleAttrValueName.trim() === "") {
			console.log("属性不能为空")
			return
		}
		// 判重
		if (
			record.spuSaleAttrValueList
				.map((it) => it.saleAttrValueName)
				.includes(saleAttrValueName)
		) {
			console.log("属性不能重复")
			return
		}
		const index = (record.order as number) - 1
		setTableData((data) => {
			data[index].flag = false
			data[index].spuSaleAttrValueList.push({
				baseSaleAttrId: data[index]["baseSaleAttrId"],
				saleAttrValueName,
			})
			return cloneDeep(data)
		})
	}
	// edit callback
	const toEdit = (record: TableItem) => {
		console.log(record)
		const index = (record.order as number) - 1
		setTableData((data) => {
			data[index].flag = true
			return cloneDeep(data)
		})
	}
	// upload 添加按钮
	const uploadButton = (
		<div>
			<PlusOutlined />
			<div style={{ marginTop: 8 }}>Upload</div>
		</div>
	)
	return (
		<Form
			onFinish={onFinish}
			onFinishFailed={onFinishFailed}
			form={form}
			name="update_spu"
			labelCol={{ flex: "100px" }}
		>
			<Form.Item label="SPU名称" name="spuName">
				<Input />
			</Form.Item>
			<Form.Item label="SPU品牌" name="tmId">
				<Select
					options={tradeMarkList.map((item) => ({
						label: item.tmName,
						value: item.id,
					}))}
				/>
			</Form.Item>
			<Form.Item label="SPU描述" name="description">
				<TextArea rows={4} />
			</Form.Item>
			<Form.Item label="SPU照片">
				<Upload
					className="upload"
					action="/api/admin/product/fileUpload"
					listType="picture-card"
					fileList={fileList}
					onPreview={uploadHandlePreview}
					onChange={uploadHandleChange}
				>
					{uploadButton}
				</Upload>
				<Modal
					open={previewOpen}
					title={previewTitle}
					footer={null}
					onCancel={uploadHandleCancel}
				>
					<img alt="example" style={{ width: "100%" }} src={previewImage} />
				</Modal>
			</Form.Item>
			<Form.Item label="SPU销售属性">
				<Select
					onChange={selectHandleChange}
					placeholder={`还可以添加${unSelectSaleAttrValueList.length}个属性值`}
					value={selectAttrValue}
					options={unSelectSaleAttrValueList.map((it) => ({
						label: it.name,
						value: `${it.id}:${it.name}`,
					}))}
				/>
				<Button
					style={{ margin: "10px 0" }}
					type="primary"
					icon={<PlusOutlined />}
					disabled={selectAttrValue ? false : true}
					onClick={addAttrValue}
				>
					添加属性值
				</Button>
				<Table dataSource={tableData} bordered pagination={false}>
					<Column title="序号" dataIndex="order" key="order" align="center" />
					<Column
						title="销售属性名称"
						dataIndex="saleAttrName"
						key="saleAttrName"
						align="center"
					/>
					<Column
						title="销售属性值"
						dataIndex="spuSaleAttrValueList"
						key="spuSaleAttrValueList"
						align="center"
						render={(_, record: TableItem) => (
							<>
								{record.spuSaleAttrValueList.map((it, i) => {
									return (
										<Tag
											closeIcon={<CloseCircleOutlined />}
											key={nanoid()}
											onClose={(e) => removeTag(e, record, i)}
										>
											{it.saleAttrValueName}
										</Tag>
									)
								})}
								{record.flag ? (
									<Input
										onBlur={(e) => toPreview(e, record)}
										size="small"
										style={{ width: "100px" }}
										ref={(el) => {
											if (el) {
												el.focus()
											}
										}}
									/>
								) : (
									<Button
										onClick={() => toEdit(record)}
										type="primary"
										icon={<PlusOutlined />}
										size="small"
									></Button>
								)}
							</>
						)}
					/>
					<Column
						title="操作"
						dataIndex="action"
						key="action"
						align="center"
						render={(_, record: TableItem) => (
							<Button
								type="primary"
								onClick={() =>
									setTableData((data) => {
										data.splice((record.order as number) - 1, 1)
										return cloneDeep(data)
									})
								}
								icon={<DeleteOutlined />}
								danger
							></Button>
						)}
					/>
				</Table>
			</Form.Item>
			<Form.Item>
				<Space>
					<Button type="primary" htmlType="submit">
						Submit
					</Button>
					<Button type="primary" onClick={close}>
						取消
					</Button>
				</Space>
			</Form.Item>
		</Form>
	)
}

export default UpdateSpu
