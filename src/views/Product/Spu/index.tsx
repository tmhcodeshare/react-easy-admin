import React, { memo, useEffect, useState } from "react"
import { Card, Button, Table, Popconfirm } from "antd"
import {
	PlusOutlined,
	EditOutlined,
	EyeOutlined,
	DeleteOutlined,
} from "@ant-design/icons"
import MainTop2 from "@/components/MainTop2"
import { useAppSelector } from "@/store"
import { shallowEqual } from "react-redux"
import { authPermission } from "@/hooks/directives"
import useCategoryFlag from "@/hooks/useCategoryFlag"
// 引入接口
import {
	reqHasSpuList,
	reqAddSpu,
	reqEditSpu,
	reqDeleteSpu,
} from "@/api/productions/spu"
import { reqAddSku } from "@/api/productions/sku"
// 引入ts类型
import type { Spu as SPU } from "@/api/productions/spu/type"
import UpdateSpu from "./UpdateSpu"
import AddSku from "./AddSku"
interface TableItem extends SPU {
	order?: number
	key?: string
	open?: boolean
}
const { Column } = Table
const Spu: React.FC = () => {
	// 封装接口
	const getSpuList = (page: number, limit: number, category3Id: number) => {
		setLoading(true)
		return reqHasSpuList(page, limit, category3Id)
			.then((res) => {
				if (res.code === 200) {
					setTotal(res.data.total)
					setSpuList(res.data.records)
				}
			})
			.catch((err) => console.dir(err))
			.finally(() => setLoading(false))
	}
	const updateSpu = async (spuParams: SPU) => {
		try {
			const { id } = spuParams
			const res =
				id !== "" ? await reqEditSpu(spuParams) : await reqAddSpu(spuParams)
			if (res.code === 200) {
				getSpuList(page, limit, spuParams.category3Id as number)
			}
		} catch (error) {
			console.dir(error)
		}
	}
	const addSku = (params: any) => {
		reqAddSku(params)
			.then((res) => {
				if (res.code === 200) {
					alert("添加成功")
				} else {
					alert("添加失败")
				}
			})
			.catch((err) => {
				console.log(err)
			})
	}
	const removeSpu = (id: number) => {
		reqDeleteSpu(id).then((res) => {
			if (res.code === 200) {
				spuList.length <= 1 && page * limit === total
					? setPage(page - 1)
					: getSpuList(page, limit, category3Id as number)
			}
		})
	}
	// store
	const { category3Id } = useAppSelector(
		(state) => ({
			category3Id: state.categorySore.category3Id,
		}),
		shallowEqual,
	)
	const initalRowData: TableItem = {
		id: "",
		category3Id,
		spuName: "",
		tmId: "",
		description: "",
		spuImageList: [],
		spuSaleAttrList: [],
	}
	// table hooks
	const [spuList, setSpuList] = useState<SPU[]>([])
	const [loading, setLoading] = useState(false)
	const [rowData, setRowData] = useState<TableItem>(initalRowData)
	// show hooks
	const [scence, setScence] = useState<number>(0)
	// pagination hooks
	const [total, setTotal] = useState(0)
	const [page, setPage] = useState(1)
	const [limit, setLimit] = useState(3)
	// MainTop2组件是否更新状态 hook
	// MainTop2组件是否更新状态 hook
	const flag = useCategoryFlag()
	useEffect(() => {
		if (flag && category3Id !== "") {
			getSpuList(page, limit, category3Id as number)
		}
	}, [flag, category3Id, page, limit])
	const open = (scence: number, record: TableItem) => {
		setScence(scence)
		setRowData(record)
	}
	const close = () => {
		setScence(0)
		setRowData(initalRowData)
	}
	return (
		<>
			<MainTop2 flag={flag} isDisabled={scence} />
			<Card style={{ marginTop: "20px" }}>
				{scence === 0 && (
					<>
						<Button
							type="primary"
							disabled={category3Id === ""}
							icon={<PlusOutlined />}
							style={authPermission("btn.Spu.add")}
							onClick={() => open(1, initalRowData)}
						>
							添加SPU
						</Button>
						<Table
							bordered
							style={{ marginTop: "20px" }}
							loading={loading}
							dataSource={spuList.map((it, i) => {
								return { ...it, order: i + 1, key: it.id }
							})}
							pagination={{
								showQuickJumper: true,
								showSizeChanger: true,
								total: total,
								position: ["bottomRight"],
								pageSize: limit,
								current: page,
								pageSizeOptions: [3, 5, 7, 9],
								onChange(page, pageSize) {
									setSpuList([])
									setPage(page)
									setLimit(pageSize)
								},
								showTotal(total) {
									return `Total ${total} items`
								},
							}}
						>
							<Column title="#" dataIndex="order" key="order" align="center" />
							<Column
								title="SPU名称"
								dataIndex="spuName"
								key="spuName"
								align="center"
							/>
							<Column
								title="SPU描述"
								dataIndex="description"
								key="description"
								align="center"
							/>
							<Column
								title="SPU操作"
								dataIndex="Action"
								key="Action"
								align="center"
								render={(_, record: TableItem) => (
									<div
										style={{
											display: "flex",
											gap: "8px",
											justifyContent: "center",
										}}
									>
										<Button
											type="primary"
											icon={<PlusOutlined />}
											style={authPermission("btn.Spu.addsku")}
											onClick={() => open(2, record)}
										>
											添加SKU
										</Button>
										<Button
											type="primary"
											icon={<EditOutlined />}
											style={authPermission("btn.Spu.update")}
											onClick={() => open(1, record)}
										>
											编辑SPU
										</Button>
										<Button
											type="primary"
											icon={<EyeOutlined />}
											style={authPermission("btn.Spu.skus")}
										>
											查看SKU列表
										</Button>
										<Popconfirm
											title="温馨提示！！！"
											description="确认要删除该项吗？"
											open={record?.open}
											onConfirm={() => removeSpu(record.id as number)}
										>
											<Button
												type="primary"
												style={authPermission("btn.Spu.delete")}
												icon={<DeleteOutlined />}
												danger
											>
												删除SPU
											</Button>
										</Popconfirm>
									</div>
								)}
							/>
						</Table>
					</>
				)}
				{scence === 1 && (
					<UpdateSpu
						finished={(val: SPU) => updateSpu(val)}
						initalValue={rowData}
						close={close}
					/>
				)}
				{scence === 2 && (
					<AddSku
						finished={(val: SPU) => addSku(val)}
						close={close}
						initalValue={rowData}
					/>
				)}
			</Card>
		</>
	)
}
export default memo(Spu)
