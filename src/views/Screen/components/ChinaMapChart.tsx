import { useEcharts } from "@/hooks/useEcharts"
import type { EChartsOption } from "echarts"
import echarts from "@/utils/echarts"
import mapJson from "../assets/china.json"
import "./ChinaMapChart.less"

const ChinaMapChart = () => {
	echarts.registerMap("china", mapJson as any)
	/* echarts sysmbol */
	let planePath: string =
		"path://M1705.06,1318.313v-89.254l-319.9-221.799l0.073-208.063c0.521-84.662-26.629-121.796-63.961-121.491c-37.332-0.305-64.482,36.829-63.961,121.491l0.073,208.063l-319.9,221.799v89.254l330.343-157.288l12.238,241.308l-134.449,92.931l0.531,42.034l175.125-42.917l175.125,42.917l0.531-42.034l-134.449-92.931l12.238-241.308L1705.06,1318.313z"
	let data = [
		{
			fromName: "北京",
			toName: "上海",
			coords: [
				[116.4551, 40.2539],
				[121.4648, 31.2891],
			],
		},
		{
			fromName: "上海",
			toName: "北京",
			coords: [
				[121.4648, 31.2891],
				[116.4551, 40.2539],
			],
		},
		{
			fromName: "北京",
			toName: "广州",
			coords: [
				[116.4551, 40.2539],
				[113.5107, 23.2196],
			],
		},
		{
			fromName: "广州",
			toName: "北京",
			coords: [
				[113.5107, 23.2196],
				[116.4551, 40.2539],
			],
		},
		{
			fromName: "北京",
			toName: "成都",
			coords: [
				[116.4551, 40.2539],
				[103.9526, 30.7617],
			],
		},
		{
			fromName: "成都",
			toName: "北京",
			coords: [
				[103.9526, 30.7617],
				[116.4551, 40.2539],
			],
		},
		{
			fromName: "成都",
			toName: "新疆维吾尔自治区",
			coords: [
				[103.9526, 30.7617],
				[85.294711, 41.371801],
			],
		},
		{
			fromName: " 新疆维吾尔自治区",
			toName: "成都",
			coords: [
				[85.294711, 41.371801],
				[103.9526, 30.7617],
			],
		},
	]
	// 各省份旅游人数 数据name要和地图JSON数据上的的区域名称一致，不然就会报错
	const dataList = [
		{ name: "澳门特别行政区", value: 18 },
		{ name: "香港特别行政区", value: 273 },
		{ name: "台湾省", value: 153 },
		{ name: "新疆维吾尔自治区", value: 76 },
		{ name: "宁夏回族自治区", value: 75 },
		{ name: "青海省", value: 18 },
		{ name: "甘肃省", value: 134 },
		{ name: "陕西省", value: 248 },
		{ name: "西藏自治区", value: 1 },
		{ name: "云南省", value: 176 },
		{ name: "贵州省", value: 146 },
		{ name: "四川省", value: 543 },
		{ name: "重庆市", value: 576 },
		{ name: "海南省", value: 168 },
		{ name: "广西壮族自治区", value: 254 },
		{ name: "广东省", value: 1407 },
		{ name: "湖南省", value: 1018 },
		{ name: "湖北省", value: 67800 },
		{ name: "河南省", value: 1273 },
		{ name: "山东省", value: 765 },
		{ name: "江西省", value: 936 },
		{ name: "福建省", value: 307 },
		{ name: "安徽省", value: 990 },
		{ name: "浙江省", value: 1237 },
		{ name: "江苏省", value: 633 },
		{ name: "上海市", value: 394 },
		{ name: "黑龙江省", value: 484 },
		{ name: "吉林省", value: 93 },
		{ name: "辽宁省", value: 126 },
		{ name: "内蒙古自治区", value: 75 },
		{ name: "山西省", value: 133 },
		{ name: "河北省", value: 319 },
		{ name: "天津市", value: 137 },
		{ name: "北京市", value: 512 },
		{ name: "南海诸岛", value: 600 },
	]

	const option: EChartsOption = {
		// 提示悬浮窗
		tooltip: {
			show: true,
			trigger: "item", // 触发形式
			backgroundColor: "transparent",
			textStyle: {
				color: "#fff",
			},
			formatter: function (params: any) {
				return params.seriesName + "<br />" + params.name + "：" + params.value
			},
		},
		// echarts大小位置
		grid: {
			left: "0px",
			right: "80px",
			top: "10px",
			bottom: "10px",
		},
		// 地理坐标系
		geo: [
			{
				zlevel: 99,
				map: "china", // 哪国地图
				// 地块标题
				label: {
					show: true, // 是否显示
					fontSize: 14, // 字体大小
					color: "rgb(255,255,255)",
				},
				// 地块样式
				itemStyle: {
					areaColor: "#12235c", // 背景颜色
					borderColor: "#2ab8ff", //边框颜色
					borderWidth: 0.5, //边框宽度
					shadowColor: "rgba(0,54,255, 0.4)", // 边框阴影颜色
					shadowBlur: 100,
				},
				emphasis: {
					//地块激活（高亮）时的样式
					itemStyle: {
						areaColor: "rgba(0,254,233,0.6)", // 背景颜色
					},
					label: {
						color: "#fff",
						fontSize: 20,
					},
				},
				// 布局
				top: 0,
				bottom: 0,
			},
		],
		// 要显示的散点数据
		series: [
			// {
			// 	name: "",
			// 	type: "lines",
			// 	coordinateSystem: "geo",
			// 	zlevel: 1,
			// 	effect: {
			// 		show: true,
			// 		period: 6,
			// 		trailLength: 0.7,
			// 		color: "red", //arrow箭头的颜色
			// 		symbolSize: 3,
			// 	},
			// 	lineStyle: {
			// 		color: "#fff",
			// 		width: 0,
			// 		curveness: 0.2,
			// 	},
			// 	data,
			// },
			// 飞机航线
			{
				zlevel: 99,
				name: "",
				type: "lines",
				effect: {
					show: true,
					period: 5, //箭头指向速度，值越小速度越快
					trailLength: 0, //特效尾迹长度[0,1]值越大，尾迹越长重
					// symbol: 'arrow', //箭头图标
					symbol: planePath,
					symbolSize: 15, //图标大小
				},
				lineStyle: {
					color: "#00eaff",
					width: 1, //尾迹线条宽度
					opacity: 0.7, //尾迹线条透明度
					curveness: 0.3, //尾迹线条曲直度
				},
				data, // 航线起始点数据列表
			},
			// 各省份旅游人数分布
			{
				name: "旅客数量",
				type: "map",
				map: "china", //注册的地图名称
				geoIndex: 0,
				data: dataList,
			},
		],
	}

	const [echartsRef] = useEcharts(option, data)

	return (
		<div className="content-box">
			<div className="map-ball"></div>
			<div ref={echartsRef} style={{ width: "100%", height: "100%" }}></div>
		</div>
	)
}

export default ChinaMapChart
