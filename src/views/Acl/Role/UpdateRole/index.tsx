import React from "react"
import { Form, Input, Modal, Button, Space } from "antd"
import type { Role } from "@/api/acl/role/type"
interface Iprops {
	show: boolean
	close: any
	finished: any
	initialValues: Role
}
const UpdateRole: React.FC<Iprops> = (props) => {
	console.log("UpdateRole渲染了")
	const { show, close, initialValues, finished } = props

	// 搜索框相关回调
	const onFinish = (values: any) => {
		finished({ ...initialValues, ...values })
		close()
	}
	return (
		<Modal title="tips" open={show} onCancel={close} footer={null}>
			<Form
				name="update_role"
				initialValues={initialValues}
				onFinish={onFinish}
				style={{ justifyContent: "space-between" }}
			>
				<Form.Item<{ roleName: string }>
					name="roleName"
					label="角色名称"
					rules={[
						{
							required: true,
							message: "角色名称必输",
							validateTrigger: "blur",
						},
					]}
				>
					<Input placeholder="请输入角色名称" />
				</Form.Item>
				<Form.Item style={{ textAlign: "right" }}>
					<Space>
						<Button onClick={close}>取消</Button>
						<Button type="primary" htmlType="submit">
							确认
						</Button>
					</Space>
				</Form.Item>
			</Form>
		</Modal>
	)
}

export default UpdateRole
