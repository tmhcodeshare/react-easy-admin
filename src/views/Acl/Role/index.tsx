import React, { memo, useCallback, useEffect, useState } from "react"
import { Button, Card, Table, Popconfirm, message } from "antd"
import {
	PlusOutlined,
	UserOutlined,
	EditOutlined,
	DeleteOutlined,
} from "@ant-design/icons"
import { authPermission } from "@/hooks/directives"
import { reqRoleList, reqAddOrEditRole, reqDeleteRole } from "@/api/acl/role"
import { reqSetPermission } from "@/api/acl/permission"
import AssignPermission from "./AssignPermission"
import UpdateRole from "./UpdateRole"
import MainTop1 from "@/components/MainTop1"
import type { Role } from "@/api/acl/role/type"
import store from "@/store"
import useGetUserInfo from "@/hooks/useUerInfo"
import "./index.less"
interface TableData extends Role {
	order: number
	key: number
}
const { Column } = Table
const initialRecod = {
	createTime: "",
	id: "",
	remark: null,
	roleName: "",
	updateTime: "",
}
// 返回当前角色
const getRoleName = (id: number, roleList: TableData[]) => {
	return roleList.find((it) => it.id === id)?.roleName
}
// 判断当前用户拥有的角色列表是否包含当前修改角色的方法
const userHasCurRole = (id: number, roleList: TableData[]) => {
	const userRoleList = store?.getState()?.userStore.userInfo?.roles
	const curRole = roleList.find((it) => it.id === id)?.roleName
	return curRole && userRoleList ? userRoleList.includes(curRole) : false
}
const Role: React.FC = () => {
	// api upper
	// getRoles
	const getRoleList = (page: number, limit: number, roleName: string) => {
		setLoading(true)
		reqRoleList(page, limit, roleName).then((res) => {
			if (res.code === 200) {
				setLoading(false)
				if (res.data && res.data.records) {
					setRoleList(
						res?.data.records.map((it: Role, index: number) => {
							const item: TableData = {
								...it,
								order: index + 1,
								key: it.id as number,
							}
							return item
						}),
					)
					setTotal(res.data.total)
				}
			}
		})
	}
	// updateRole
	const updateRo = (value: any) => {
		reqAddOrEditRole(value)
			.then((res) => {
				if (res.code === 200) {
					message.success({ content: res.message })
					// 重载网页，获取用户最先菜单权限
					if (isUserHasRole) {
						setIsUserHasRole(false)
						message.info(
							`当前用户拥有的“${value.roleName}”角色已被系统修改，重新获取用户信息`,
							10,
						)
						setIsFresh(true)
					}
					getRoleList(page, limit, roleName)
				} else {
					message.error({ content: res.message })
				}
				setIsFresh(false)
			})
			.catch((error) => {
				message.error({ content: error.message })
			})
	}
	// setPermission
	const setPermission = (id: number, list: React.Key[]) => {
		const permissionIdList = list.map((it) => Number(it))
		reqSetPermission({ permissionIdList, roleId: id })
			.then((res) => {
				if (res.code === 200) {
					message.success({ content: res.message })
					// 重载网页，获取用户最先菜单权限
					if (isUserHasRole) {
						setIsUserHasRole(false)
						message.info(
							`当前用户拥有的“${getRoleName(
								id,
								roleList,
							)}”角色权限已被系统修改，重新获取用户信息`,
							10,
						)
						setIsFresh(true)
					}
				} else {
					message.error({ content: res.message })
				}
			})
			.catch((error) => {
				message.error({ content: error.message })
			})
	}
	// removeRole
	const removeRole = (id: number) => {
		reqDeleteRole(id)
			.then((res) => {
				if (res.code === 200) {
					message.success({ content: res.message })
					// 重载网页，获取用户最先菜单权限
					if (isUserHasRole) {
						setIsUserHasRole(false)
						message.info(
							`当前用户拥有的“${getRoleName(
								id,
								roleList,
							)}”角色已被系统删除，重新获取用户信息`,
							10,
						)
						setIsFresh(true)
					}
					roleList.length <= 1 && page * limit === total
						? setPage(page - 1)
						: getRoleList(page, limit, roleName)
				} else {
					message.error({ content: res.message })
				}
				setIsFresh(false)
			})
			.catch((error) => {
				message.error({ content: error.message })
			})
	}
	// hooks
	// MainTop1 state
	const [roleName, setRoleName] = useState("")
	// Table state
	const [roleList, setRoleList] = useState<TableData[]>([])
	const [loading, setLoading] = useState<boolean>(false)
	// pagination state
	const [page, setPage] = useState<number>(1)
	const [limit, setLimit] = useState<number>(5)
	const [total, setTotal] = useState<number>(0)
	// Drawer state
	const [open, setOpen] = useState<boolean>(false)
	const [roleId, setRoleId] = useState<number | string>("")
	// Modal state
	const [isModalOpen, setIsModalOpen] = useState(false)
	const [rowData, setRowData] = useState<Role>(initialRecod)
	// user has role ?
	const [isUserHasRole, setIsUserHasRole] = useState<boolean>(false)
	const setIsFresh = useGetUserInfo()
	// watcher
	useEffect(() => {
		getRoleList(page, limit, roleName)
	}, [limit, page, roleName])
	// MainTop1 callback
	const mainTop1Finished = useCallback(
		(values: any) => setRoleName(values.roleName),
		[setRoleName],
	)
	// Drawer callbacks
	const openDrawer = (roleId: number) => {
		setRoleId(roleId)
		setIsUserHasRole(userHasCurRole(roleId, roleList))
		setOpen(true)
	}
	const colseDrawer = () => {
		setRoleId("")
		setOpen(false)
	}
	const UpdateRoleFinished = useCallback((val: any) => {
		updateRo(val)
	}, [])
	// Modal callbacks
	const showModal = (record: any) => {
		setRowData(record)
		setIsUserHasRole(userHasCurRole(record.id, roleList))
		setIsModalOpen(true)
	}
	const closeModal = () => {
		setRowData(initialRecod)
		setIsModalOpen(false)
	}
	const confirmHandleOk = (record: any) => {
		setIsUserHasRole(userHasCurRole(record.id, roleList))
		console.log(record)
		removeRole(record.id)
	}
	return (
		<div>
			<MainTop1
				formName="search_role"
				initialValues={roleName}
				name="roleName"
				label="角色名称"
				finshed={mainTop1Finished}
			/>
			<Card style={{ marginTop: "20px" }}>
				<div style={{ textAlign: "left" }}>
					<Button
						type="primary"
						onClick={() => showModal(initialRecod)}
						icon={<PlusOutlined />}
					>
						添加角色
					</Button>
				</div>
				<Table
					style={{ marginTop: "20px" }}
					bordered
					dataSource={roleList}
					loading={loading}
					pagination={{
						showQuickJumper: true,
						showSizeChanger: true,
						total: total,
						position: ["bottomRight"],
						pageSize: limit,
						current: page,
						pageSizeOptions: [5, 10, 15, 20],
						onChange(page, pageSize) {
							setRoleList([])
							setPage(page)
							setLimit(pageSize)
						},
						showTotal(total) {
							return `Total ${total} items`
						},
					}}
				>
					<Column title="#" dataIndex="order" key="order" align="center" />
					<Column title="id" dataIndex="id" key="id" align="center" />
					<Column
						title="角色名称"
						dataIndex="roleName"
						key="roleName"
						align="center"
					/>
					<Column
						title="创建时间"
						dataIndex="createTime"
						key="createTime"
						align="center"
					/>
					<Column
						title="更新时间"
						dataIndex="updateTime"
						key="updateTime"
						align="center"
					/>
					<Column
						title="操作"
						key="action"
						align="center"
						render={(_, record: any) => (
							<div className="btns">
								<Button
									style={authPermission("btn.Role.assgin")}
									icon={<UserOutlined />}
									type="primary"
									onClick={() => openDrawer(record.id)}
								>
									分配权限
								</Button>
								<Button
									style={authPermission("btn.Role.update")}
									icon={<EditOutlined />}
									type="primary"
									onClick={() => showModal(record)}
								>
									编辑
								</Button>
								<Popconfirm
									title="温馨提示！！！"
									description="确认要删除该项吗？"
									open={record?.open}
									onConfirm={() => confirmHandleOk(record)}
								>
									<Button
										style={authPermission("btn.Role.remove")}
										icon={<DeleteOutlined />}
										type="primary"
									>
										删除
									</Button>
								</Popconfirm>
							</div>
						)}
					/>
				</Table>
			</Card>
			{open && (
				<AssignPermission
					isUserHasRole={isUserHasRole}
					id={roleId}
					open={open}
					finished={(id: number, list: React.Key[]) => setPermission(id, list)}
					close={colseDrawer}
				/>
			)}
			{isModalOpen && (
				<UpdateRole
					show={isModalOpen}
					initialValues={rowData}
					finished={UpdateRoleFinished}
					close={closeModal}
				/>
			)}
		</div>
	)
}
export default memo(Role)
