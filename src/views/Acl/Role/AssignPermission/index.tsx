import React, { useEffect, useState } from "react"
import { Drawer, Tree, Button } from "antd"
import type { DataNode, TreeProps } from "antd/es/tree"
import { reqPermissionMenuOrRolePermissionMenu } from "@/api/acl/permission"
import type { MenuItem } from "@/api/acl/permission/type"
import "./index.less"
interface Iprops {
	open: boolean
	close: any
	id: number | string
	isUserHasRole: boolean
	finished: any
}
// 定义整理权限列表函数
const arrangeData = (origin: MenuItem[]): DataNode[] => {
	return origin.map((it: MenuItem) => {
		const obj: DataNode = { key: `${it.id}`, title: it.name }
		if (it.children && it.children.length > 0) {
			obj.children = arrangeData(it.children)
		}
		return obj
	})
}
// 定义筛选权限函数
const filterSelect = (target: React.Key[], origin: MenuItem[]) => {
	origin.forEach((it: MenuItem) => {
		if (it.children === null || it.children.length === 0) {
			// 没有子集
			it.select && target.push(`${it.id}`)
		} else {
			// 有子集
			filterSelect(target, it.children)
		}
	})
	if (target.length === 0 && origin[0].select) target.push(1) // 超级管理员
}
const AssignPermission: React.FC<Iprops> = (props) => {
	const { open, close, id, finished } = props
	const [data, setDate] = useState<DataNode[]>([])
	const [checkedKeys, setCheckedKeys] = useState<React.Key[]>([])
	const onCheck: TreeProps["onCheck"] = (checkedKeys: any) => {
		setCheckedKeys(checkedKeys)
	}
	useEffect(() => {
		reqPermissionMenuOrRolePermissionMenu(id).then((res) => {
			if (res.code === 200) {
				setDate(arrangeData(res.data as MenuItem[]))
				setCheckedKeys((val) => {
					filterSelect(val, res.data as MenuItem[])
					return val
				})
			}
		})
	}, [id])
	const submit = () => {
		if (id !== "") {
			finished(id as number, checkedKeys)
			close()
		}
	}
	return (
		<Drawer title="分配角色权限" placement="right" onClose={close} open={open}>
			<div className="tree">
				{data.length > 0 && (
					<Tree
						checkable
						defaultExpandAll={true}
						checkedKeys={checkedKeys}
						onCheck={onCheck}
						treeData={data}
					/>
				)}
			</div>
			<div style={{ textAlign: "right", paddingRight: "10px" }}>
				<Button style={{ marginRight: "10px" }} onClick={close}>
					取消
				</Button>
				<Button type="primary" disabled={data.length === 0} onClick={submit}>
					确认
				</Button>
			</div>
		</Drawer>
	)
}
export default AssignPermission
