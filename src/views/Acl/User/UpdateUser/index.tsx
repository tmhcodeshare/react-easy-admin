import React, { useEffect, useMemo, useState, memo } from "react"
import { Drawer, Form, Input, Checkbox, Row, Col, Button } from "antd"
import type { TableItem } from "../index"
import { reqRoleList } from "@/api/acl/user"
import type { Role } from "@/api/acl/user/type"
import type { CheckboxValueType } from "antd/es/checkbox/Group"
import "./index.less"
interface Iprops {
	open: boolean
	scene: number
	close: any
	finished: any
	initialValues: TableItem
}
const titles = ["添加用户", "分配用户角色", "编辑用户"]
const forms = [0, 2]
// 在使用form管理状态时，可以直接使用传入的props值initialValues
// 本质上，form的setFieldsValue方法不会修改props值initialValues，保证了单向数据流
const UpdateUser: React.FC<Iprops> = (props) => {
	console.log("UpdateUser渲染了")
	const { open, scene, close, initialValues, finished } = props
	// 封装接口
	const getRoleList = (id: number) => {
		reqRoleList(id).then((res) => {
			if (res.code === 200) {
				setAllRolesList(res.data.allRolesList)
				const list = res.data.assignRoles.map((it) => it.id)
				setChecks(list)
			}
		})
	}
	// hooks checkbox
	const [allRolesList, setAllRolesList] = useState<Role[]>([])
	const [checks, setChecks] = useState<Array<number>>([])
	useEffect(() => {
		if (scene === 1) {
			const { id } = initialValues
			id !== "" && getRoleList(id as number)
		} else {
			form.setFieldsValue(initialValues)
		}
	}, [scene])
	useEffect(() => {
		form.setFieldsValue({
			userId: initialValues.id,
			username: initialValues.username,
			"role-id-list": checks,
		})
	}, [checks])
	const [form] = Form.useForm()
	const title = useMemo(() => titles[scene], [scene])
	// form callbacks
	const onFinish = (values: any) => {
		console.log("Success:", values)
		finished(values)
		closeHandle()
	}
	const onFinishFailed = (errorInfo: any) => {
		console.log("Failed:", errorInfo)
	}
	// check all callback
	const checkAll = (e: any) => {
		const list = e.target.checked ? allRolesList.map((it) => it.id) : []
		setChecks(list)
	}
	// checkbox group callback
	const checkChange = (checkedValues: CheckboxValueType[]) => {
		// console.log("checked = ", checkedValues)
		setChecks(checkedValues as number[])
	}
	// Drawer callback
	const closeHandle = () => {
		close()
		form.setFieldsValue(null)
	}
	return (
		<Drawer
			className="updateUser"
			title={title}
			open={open}
			onClose={closeHandle}
			footer={
				<div style={{ textAlign: "end" }}>
					<Button onClick={closeHandle} className="footCancel">
						取消
					</Button>
					<Button type="primary" onClick={form.submit}>
						确认
					</Button>
				</div>
			}
		>
			<Form
				form={form}
				name="update_user"
				onFinish={onFinish}
				onFinishFailed={onFinishFailed}
				autoComplete="off"
			>
				<Form.Item
					label="用户名称"
					name="username"
					validateTrigger="onBlur"
					rules={[
						{
							required: scene !== 1,
							message: "用户名称不能为空",
						},
						{
							min: 6,
							message: "用户名称不少于6字符",
						},
					]}
				>
					<Input disabled={scene === 1} />
				</Form.Item>
				{forms.includes(scene) && (
					<>
						<Form.Item
							label="用户昵称"
							name="name"
							validateTrigger="onBlur"
							rules={[
								{
									required: true,
									message: "用户昵称不能为空",
								},
								{
									min: 6,
									message: "用户昵称不少于6字符",
								},
							]}
						>
							<Input />
						</Form.Item>
						{scene === 0 && (
							<Form.Item
								label="用户密码"
								name="password"
								validateTrigger="onBlur"
								rules={[
									{
										required: true,
										message: "用户密码不能为空",
									},
									{ min: 6, max: 22, message: "密码6-22位" },
								]}
							>
								<Input />
							</Form.Item>
						)}
					</>
				)}
				{scene === 1 && (
					<>
						<Form.Item label="角色列表">
							<Checkbox
								checked={
									allRolesList.length > 0 &&
									checks.length === allRolesList.length
								}
								onClick={checkAll}
							></Checkbox>
						</Form.Item>
						<Form.Item name="role-id-list">
							<Checkbox.Group onChange={checkChange}>
								<Row>
									{allRolesList.length > 0 &&
										allRolesList.map((it) => {
											return (
												<Col key={it.id}>
													<Checkbox
														value={it.id}
														style={{ lineHeight: "32px" }}
													>
														{it.roleName}
													</Checkbox>
												</Col>
											)
										})}
								</Row>
							</Checkbox.Group>
						</Form.Item>
					</>
				)}
			</Form>
		</Drawer>
	)
}
export default memo(UpdateUser)
