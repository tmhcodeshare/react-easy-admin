import React, { memo, useEffect, useState, useCallback } from "react"
import { Button, Card, Table, Popconfirm, Space, message } from "antd"
import {
	PlusOutlined,
	UserOutlined,
	EditOutlined,
	DeleteOutlined,
} from "@ant-design/icons"
import MainTop1 from "@/components/MainTop1"
import UpdateUser from "./UpdateUser"
import { authPermission } from "@/hooks/directives"
import type { Record } from "@/api/acl/user/type"
import {
	reqUserList,
	reqAddOrEditUser,
	reqAssignRole,
	reqDeleteUser,
	reqBatchRemove,
} from "@/api/acl/user"
import store from "@/store"
import useGetUserInfo from "@/hooks/useUerInfo"
import useLogout from "@/hooks/useLogout"

export interface TableItem extends Record {
	order: number
	key: number | string
}
const { Column } = Table
const initialRecod: TableItem = {
	order: 0,
	key: "",
	createTime: "",
	id: "",
	name: "",
	password: "",
	phone: null,
	roleName: "",
	updateTime: "",
	username: "",
}
const checkSame = (username: string[]) => {
	const userInfo = store.getState().userStore.userInfo
	return userInfo && username.includes(userInfo.name)
}
const User: React.FC = () => {
	// api upper
	// getUsers
	const getUsers = async (page: number, limit: number, username: string) => {
		try {
			setLoading(true)
			let res = await reqUserList(page, limit, { username })
			if (res.code === 200) {
				setTotal(res.data.total)
				const list: TableItem[] = res.data.records.map(
					(it, index): TableItem => {
						return { ...it, order: index + 1, key: it.id }
					},
				)
				setUserList(list)
			}
		} catch (error) {
			console.dir(error)
		} finally {
			setLoading(false)
		}
	}
	const update_user = (values: any) => {
		const data = { ...rowData, ...values }
		reqAddOrEditUser(data)
			.then((res) => {
				if (res.code === 200) {
					if (checkSame([rowData.username])) {
						setIsFresh(true)
						message.info("你的用户信息已被修改，请重新登录！！！", 10)
					}
					getUsers(page, limit, username)
				} else {
					message.error({
						content: res?.message,
					})
				}
			})
			.catch((error) =>
				message.error({
					content: error?.message,
				}),
			)
	}
	const assign_role = (values: any) => {
		const data = {
			userId: rowData.id,
			roleIdList: values["role-id-list"],
		}
		reqAssignRole(data)
			.then((res) => {
				if (res.code === 200) {
					if (checkSame([rowData.username])) {
						setIsFresh(true)
						message.info("你所拥有的角色权限已被修改，请重新登录！！", 10)
					}
					getUsers(page, limit, username)
				} else {
					message.error({
						content: res?.message,
					})
				}
			})
			.catch((error) =>
				message.error({
					content: error?.message,
				}),
			)
	}
	const remove_user = async function <T>(isList: boolean, data: T) {
		try {
			const res = isList
				? await reqBatchRemove(data)
				: await reqDeleteUser(data)
			if (res.code === 200) {
				if (
					checkSame(
						isList
							? userList
									.filter((it) => selectedRowKeys.includes(it.id))
									.map((it) => it.username)
							: [rowData.username],
					)
				) {
					message.info("你的账号已被删除，请重新登录！！", 3)
					logout()
					return
				}
				userList.length <= 1 && page * limit === total
					? setPage(page - 1)
					: getUsers(page, limit, username)
			} else {
				message.error({
					content: res?.message,
				})
			}
		} catch (error) {
			console.log(error)
		}
	}
	// hooks
	const logout = useLogout()
	// MainTop1 state
	const [username, setUsername] = useState("")
	// Table state
	const [loading, setLoading] = useState<boolean>(false)
	const [userList, setUserList] = useState<TableItem[]>([])
	const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([])
	// pagination state
	const [page, setPage] = useState<number>(1)
	const [limit, setLimit] = useState<number>(5)
	const [total, setTotal] = useState<number>(0)
	// Drawer state
	const [open, setOpen] = useState<boolean>(false)
	const [scene, setScene] = useState<number>(-1)
	const [rowData, setRowData] = useState<TableItem>(initialRecod)
	// user has role ?
	const setIsFresh = useGetUserInfo()
	useEffect(() => {
		getUsers(page, limit, username)
	}, [page, limit, username])
	// MainTop1 callback
	const mainTop1Finished = useCallback(
		(values: any) => setUsername(values.roleName),
		[setUsername],
	)
	// Table callbacks
	const onSelectChange = (newSelectedRowKeys: React.Key[]) => {
		console.log("selectedRowKeys changed: ", newSelectedRowKeys)
		setSelectedRowKeys(newSelectedRowKeys)
	}
	const rowSelection = {
		selectedRowKeys,
		onChange: onSelectChange,
	}
	// Drawer callbacks
	const openDrawer = (scene: number, data: TableItem) => {
		setOpen(true)
		setScene(scene)
		setRowData(data)
	}
	const closeDrawer = useCallback(() => {
		setOpen(false)
		setScene(-1)
		setRowData(initialRecod)
	}, [])
	const UpdateUserFinished = useCallback((values: any) => {
		scene >= 0 && scene !== 1 ? update_user(values) : assign_role(values)
	}, [])
	return (
		<>
			<MainTop1
				formName="search_user"
				name="username"
				label="用户名"
				initialValues={username}
				finshed={mainTop1Finished}
			/>
			<Card style={{ marginTop: "20px" }}>
				<div style={{ textAlign: "left" }}>
					<Space>
						<Button
							type="primary"
							onClick={() => openDrawer(0, initialRecod)}
							icon={<PlusOutlined />}
						>
							添加角色
						</Button>
						<Button
							type="primary"
							disabled={!(selectedRowKeys.length > 0)}
							danger
							icon={<DeleteOutlined />}
							onClick={() => remove_user(true, selectedRowKeys)}
						>
							批量删除
						</Button>
					</Space>
				</div>
				<Table
					style={{ marginTop: "20px" }}
					bordered
					dataSource={userList}
					loading={loading}
					rowSelection={rowSelection}
					pagination={{
						showQuickJumper: true,
						showSizeChanger: true,
						total: total,
						position: ["bottomRight"],
						pageSize: limit,
						current: page,
						pageSizeOptions: [5, 7, 9, 11],
						onChange(page, pageSize) {
							setUserList([])
							setPage(page)
							setLimit(pageSize)
						},
						showTotal(total) {
							return `Total ${total} items`
						},
					}}
				>
					<Column title="#" dataIndex="order" key="order" align="center" />
					<Column title="id" dataIndex="id" key="id" align="center" />
					<Column
						title="用户名字"
						dataIndex="username"
						key="username"
						align="center"
						ellipsis={{ showTitle: true }}
					/>
					<Column
						title="用户名称"
						dataIndex="name"
						key="name"
						align="center"
						ellipsis={{ showTitle: true }}
					/>
					<Column
						title="用户角色"
						dataIndex="roleName"
						key="roleName"
						align="center"
						ellipsis={{ showTitle: true }}
					/>
					<Column
						title="创建时间"
						dataIndex="createTime"
						key="createTime"
						align="center"
						ellipsis={{ showTitle: true }}
					/>
					<Column
						title="更新时间"
						dataIndex="updateTime"
						key="updateTime"
						align="center"
						ellipsis={{ showTitle: true }}
					/>
					<Column
						title="操作"
						key="action"
						align="center"
						width="30%"
						render={(_, record: any) => (
							<div
								style={{
									display: "flex",
									justifyContent: "center",
									gap: "8px",
								}}
							>
								<Button
									style={authPermission("btn.User.assgin")}
									icon={<UserOutlined />}
									type="primary"
									onClick={() => openDrawer(1, record)}
								>
									分配角色
								</Button>
								<Button
									style={authPermission("btn.User.update")}
									icon={<EditOutlined />}
									type="primary"
									onClick={() => openDrawer(2, record)}
								>
									编辑
								</Button>
								<Popconfirm
									title="温馨提示！！！"
									description="确认要删除该项吗？"
									open={record?.open}
									onConfirm={() => {
										setRowData(record)
										remove_user(false, record.id)
									}}
								>
									<Button
										style={authPermission("btn.User.remove")}
										icon={<DeleteOutlined />}
										type="primary"
									>
										删除
									</Button>
								</Popconfirm>
							</div>
						)}
					/>
				</Table>
			</Card>
			{/* {open && (
				<UpdateUser
					initialValues={rowData}
					close={closeDrawer}
					finished={(values: any) => {
						scene >= 0 && scene !== 1
							? update_user(values)
							: assign_role(values)
					}}
					open={open}
					scene={scene}
				/>
			)} */}
			<UpdateUser
				initialValues={rowData}
				close={closeDrawer}
				finished={UpdateUserFinished}
				open={open}
				scene={scene}
			/>
		</>
	)
}
export default memo(User)
