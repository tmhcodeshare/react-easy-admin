import React, { memo, useEffect, useState } from "react"
import { Table, Button, Popconfirm, message } from "antd"
import {
	PlusCircleOutlined,
	EditOutlined,
	DeleteOutlined,
} from "@ant-design/icons"
// 引入接口
import {
	reqPermissionMenuOrRolePermissionMenu,
	reqAddOrEditPermission,
	reqDeletePermission,
} from "@/api/acl/permission"

// 引入ts类型
import type { MenuItem } from "@/api/acl/permission/type"
// 引入自定义按钮权限函数
import { authPermission } from "@/hooks/directives"
import UpdatePermission from "./UpdatePermission"
// import store from "@/store"
import "./index.less"
import { useDispatch } from "react-redux"
const { Column } = Table
interface DataType extends MenuItem {
	key: React.Key
}
type FieldType = {
	name?: string
	code?: string
	pid?: number
	level?: number
	id: number | string
}
// 整理数据函数
const arrangeData = (permissionList: MenuItem[]): DataType[] => {
	return permissionList.map((item: MenuItem) => {
		const { id, children } = item
		if (children && children.length === 0) {
			item.children = null
		}
		if (children && children.length > 0) {
			item.children = arrangeData(children)
		}
		return { ...item, key: id }
	})
}
// 初始化数据
const initialData = {
	name: "",
	code: "",
	pid: 0,
	level: 0,
	id: "",
}
const Permission: React.FC = () => {
	// 封装接口
	// 权限菜单列表
	const getPermission = async () => {
		setLoading(true)
		try {
			let res = await reqPermissionMenuOrRolePermissionMenu("")
			if (res.code === 200) {
				setPermissionList(arrangeData(res.data as MenuItem[]))
			}
		} catch (error) {
			console.log(error)
		} finally {
			setLoading(false)
		}
	}

	const updatePer = (data: any) => {
		reqAddOrEditPermission(data).then((res) => {
			if (res.code === 200) {
				setIsModalOpen(false)
				getPermission()
			} else {
				message.error({
					content: res?.message,
				})
			}
		})
	}

	// hooks
	const dispatch = useDispatch()
	const [permissionList, setPermissionList] = useState<DataType[]>([])
	const [loading, setLoading] = useState<boolean>(false)
	// 模态框相关state
	// const [userIsHasPermission, setUserIsHasPermission] = useState<boolean>(false)
	const [isModalOpen, setIsModalOpen] = useState<boolean>(false)
	const [initialValues, setInitialValues] = useState<FieldType>(initialData)
	useEffect(() => {
		getPermission()
	}, [dispatch])
	useEffect(() => {
		if (!isModalOpen) {
			setInitialValues(initialData)
		}
	}, [isModalOpen])
	// 模态框相关回调
	const showModal = (type: string, record: any) => {
		setIsModalOpen(true)
		if (type === "edit") {
			// setUserIsHasPermission(userHasCurPermission(record.code))
			setInitialValues((val) => {
				for (const key in val) {
					// @ts-expect-error aasdas
					val[key] = record[key]
				}
				return val
			})
		} else {
			setInitialValues((val) => {
				val.pid = record.id
				val.level = record.level + 1
				return val
			})
		}
	}

	const handleCancel = () => {
		setIsModalOpen(false)
	}
	// 删除按钮回调
	const confirmHandleOk = (record: any) => {
		// setUserIsHasPermission(userHasCurPermission(record.code))
		reqDeletePermission(record.id).then((res) => {
			delete record.open
			if (res.code === 200) {
				getPermission()
			} else {
				message.error({
					content: res?.data || "删除失败！！！",
				})
			}
		})
	}
	return (
		<>
			{permissionList.length > 0 && (
				<Table
					dataSource={permissionList}
					loading={loading}
					bordered
					expandable={{ defaultExpandAllRows: true }}
					pagination={{
						position: [],
					}}
				>
					<Column title="名称" dataIndex="name" key="name" width="25%" />
					<Column title="权限值" dataIndex="code" key="code" width="20%" />
					<Column
						title="更新时间"
						dataIndex="updateTime"
						key="updateTime"
						width="20%"
					/>
					<Column
						title="操作"
						key="action"
						width="35"
						render={(_, record: any) => (
							<div className="btns">
								<Button
									style={authPermission("btn.Permission.add")}
									disabled={record.level === 4}
									icon={<PlusCircleOutlined />}
									type="primary"
									onClick={() => showModal("add", record)}
								>
									{record.level === 3 ? "添加功能" : "添加菜单"}
								</Button>
								<Button
									style={authPermission("btn.Permission.update")}
									disabled={record.id === 1}
									icon={<EditOutlined />}
									type="primary"
									onClick={() => showModal("edit", record)}
								>
									编辑
								</Button>
								<Popconfirm
									title="温馨提示！！！"
									description="确认要删除该项吗？"
									open={record?.open}
									onConfirm={() => confirmHandleOk(record)}
								>
									<Button
										style={authPermission("btn.Permission.remove")}
										disabled={record.id === 1}
										icon={<DeleteOutlined />}
										type="primary"
									>
										删除
									</Button>
								</Popconfirm>
							</div>
						)}
					/>
				</Table>
			)}
			{isModalOpen && (
				<UpdatePermission
					show={isModalOpen}
					close={handleCancel}
					initialValues={initialValues}
					finished={(val: any) => updatePer(val)}
				/>
			)}
		</>
	)
}
export default memo(Permission)
