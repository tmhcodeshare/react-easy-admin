import React from "react"
import { Button, Modal, Form, Input } from "antd"
type FieldType = {
	name?: string
	code?: string
	pid?: number
	level?: number
	id: number | string
}
interface Iprops {
	show: boolean
	close: any
	initialValues: FieldType
	finished: any
}
const UpdatePermission: React.FC<Iprops> = (props) => {
	const { show, close, initialValues, finished } = props
	const onFinish = (values: any) => {
		console.log("Success:", values)
		console.log("initialValues:", initialValues)
		const data = { ...initialValues, ...values }
		finished(data)
	}
	const onFinishFailed = (errorInfo: any) => {
		console.log("Failed:", errorInfo)
	}
	return (
		<Modal title="tips" open={show} onCancel={close} footer={null}>
			<Form
				name="permission"
				initialValues={initialValues}
				labelCol={{ span: 4 }}
				wrapperCol={{ span: 20 }}
				onFinish={onFinish}
				onFinishFailed={onFinishFailed}
				autoComplete="off"
			>
				<Form.Item<FieldType>
					label="名称"
					name="name"
					rules={[
						{
							required: true,
							message: "权限名称必输",
							validateTrigger: "blur",
						},
					]}
				>
					<Input />
				</Form.Item>

				<Form.Item<FieldType>
					label="权限值"
					name="code"
					rules={[
						{
							required: true,
							message: "权限值必输",
							validateTrigger: "blur",
						},
					]}
				>
					<Input />
				</Form.Item>
				<Form.Item wrapperCol={{ offset: 17, span: 20 }}>
					<Button
						type="primary"
						className="login_btn"
						style={{ marginRight: "10px" }}
						onClick={close}
					>
						取消
					</Button>
					<Button type="primary" className="login_btn" htmlType="submit">
						确认
					</Button>
				</Form.Item>
			</Form>
		</Modal>
	)
}
export default UpdatePermission
