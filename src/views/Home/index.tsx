import React, { memo } from "react"
import { Card } from "antd"
import SvgIcon from "@/components/SvgIcon"
import { useAppSelector } from "@/store"
import { shallowEqual } from "react-redux"
import { getTimeRegion } from "@/utils"
import "./index.less"
const Home: React.FC = () => {
	const { userInfo } = useAppSelector(
		(state) => ({
			userInfo: state.userStore.userInfo,
		}),
		shallowEqual,
	)
	return (
		<>
			<Card>
				<div className="container">
					<img src={userInfo?.avatar} alt="" />
					<div className="context">
						<h1>
							{getTimeRegion()}好呀！！{userInfo?.name}
						</h1>
						<p>后台管理系统！！！</p>
					</div>
				</div>
			</Card>
			<div className="svg">
				<SvgIcon svgName="welcome.svg" size="800px" />
			</div>
		</>
	)
}
export default memo(Home)
