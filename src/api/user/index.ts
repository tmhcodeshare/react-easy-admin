import _http from "@/request/http"
import { loginForm, UserInfo } from "./type"
enum API {
	LOGIN = "/admin/acl/index/login",
	LOGOUT = "/admin/acl/index/logout",
	USERINFO = "/admin/acl/index/info",
}

// 登录接口
export const reqLogIn = (data: loginForm) =>
	_http.post<string | null, loginForm>(API.LOGIN, data)

// 用户信息接口
export const reqUserInfo = () => _http.get<UserInfo | null, any>(API.USERINFO)

// 用户登出接口
export const reqLogOut = () => _http.post<string | null, any>(API.LOGOUT)
