// 定义登录参数接口
export interface loginForm {
	username: string
	password: string
}

// 定义用户信息接口
export interface UserInfo {
	routes: string[]
	buttons: string[]
	roles: string[]
	name: string
	avatar: string
}
