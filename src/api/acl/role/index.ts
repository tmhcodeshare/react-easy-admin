import _http from "@/request/http"
// 引入ts类型
import type { RoleListData, Role } from "./type"

enum API {
	// 获取角色列表
	ROLELIST_URL = "/admin/acl/role/",
	// 新增角色
	ADDROLE_URL = "/admin/acl/role/save",
	// 修改角色
	EDITROLE_URL = "/admin/acl/role/update",
	// 删除角色
	DELETEROLE_URL = "/admin/acl/role/remove/",
	// 获取当前角色
	ROLE_URL = "/admin/acl/role/get/",
}
// 获取角色列表
export const reqRoleList = (page: number, limit: number, roleName: string) => {
	if (roleName) {
		return _http.get<RoleListData, any>(
			API.ROLELIST_URL + `${page}/${limit}/?roleName=${roleName}`,
		)
	}
	return _http.get<RoleListData, any>(API.ROLELIST_URL + `${page}/${limit}`)
}

// 更新角色
export const reqAddOrEditRole = (data: Role) => {
	if (data.id) {
		return _http.put<any, Role>(API.EDITROLE_URL, data)
	} else {
		return _http.post<any, Role>(API.ADDROLE_URL, data)
	}
}

// 删除角色
export const reqDeleteRole = (id: number) =>
	_http.delete<any, any>(API.DELETEROLE_URL + id)

// 获取当前角色
export const reqRole = (id: number) => _http.get<any, any>(API.ROLE_URL + id)
