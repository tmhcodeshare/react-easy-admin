// 角色ts类型
export interface Role {
	id: number | string
	createTime?: string
	updateTime?: string
	roleName: string
	remark?: null
}

// 获取角色列表接口返回数据 ts类型
export interface RoleListData {
	records: Role[]
	total: number
	size: number
	current: number
	orders: []
	optimizeCountSql: boolean
	hitCount: boolean
	countId?: null
	maxLimit?: null
	searchCount: boolean
	pages: number
}
