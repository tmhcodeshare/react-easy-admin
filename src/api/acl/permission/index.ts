import _http from "@/request/http"

// 引入ts类型
import type { MenuItem, UpdateParams, SetParams } from "./type"

// 枚举接口
enum API {
	// 获取权限菜单
	PERMISSIONMENU_URL = "/admin/acl/permission",
	// 根据角色获取权限菜单
	ROLEPERMISSIONMENU_URL = "/admin/acl/permission/toAssign/",
	// 给角色分配权限
	SETPERMISSION_URL = "/admin/acl/permission/doAssignAcl",
	// 新增菜单
	ADDPERMISSION_URL = "/admin/acl/permission/save",
	// 修改菜单
	EDITPERMISSION_URL = "/admin/acl/permission/update",
	// 删除权限
	DELETEPERMISSION_URL = "/admin/acl/permission/remove/",
}

// 获取权限菜单
export const reqPermissionMenuOrRolePermissionMenu = (id: number | string) => {
	if (id) {
		return _http.get<MenuItem[], any>(API.ROLEPERMISSIONMENU_URL + id)
	} else {
		return _http.get<MenuItem[], any>(API.PERMISSIONMENU_URL)
	}
}

// 更新菜单权限
export const reqAddOrEditPermission = (data: UpdateParams) => {
	if (data.id !== "") {
		// 修改
		return _http.put<any, UpdateParams>(API.EDITPERMISSION_URL, data)
	} else {
		// 新增
		return _http.post<any, UpdateParams>(API.ADDPERMISSION_URL, data)
	}
}

// 删除菜单权限
export const reqDeletePermission = (id: number) =>
	_http.delete<any, any>(API.DELETEPERMISSION_URL + id)

// 给角色分配权限
export const reqSetPermission = (data: SetParams) =>
	_http.post<any, SetParams>(API.SETPERMISSION_URL, data)
