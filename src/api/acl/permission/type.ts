// 菜单项 ts类型
export interface MenuItem {
	id: number
	createTime: string
	updateTime: string
	pid: number
	name: string
	code: string
	toCode: string
	type: number
	status: null
	level: number
	children: MenuItem[] | null
	select: false
}

// 给角色分配权限接口参数 ts类型
export interface SetParams {
	permissionIdList: number[]
	roleId: number
}

// 更新菜单接口参数ts类型
export interface UpdateParams {
	id: number | string
	name: string
	code: string
	level: number
	pid: number
}
