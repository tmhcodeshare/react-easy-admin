interface Common {
	code: number
	message: string
	ok: boolean
}

// 无返回data数据 ts类型
export interface UserCommon extends Common {
	data: null
}

// 用户信息 ts类型
export interface Record {
	id: number | string // 用户id
	createTime: string
	updateTime: string
	username: string // 用户名称
	password: string // 用户密码
	name: string // 用户名字
	phone: null
	roleName: string // 用户角色
}

// 用户列表 ts类型
export type Records = Record[]

// 获取用户列表接口返回数据  ts类型
export interface UserListData {
	records: Records
	total: number
	size: number
	current: number
	pages: number
	orders: []
	optimizeCountSql: boolean
	hitCount: boolean
	countId: null
	maxLimit: null
	searchCount: boolean
}

// 角色ts类型
export interface Role {
	id: number
	createTime?: string
	updateTime?: string
	roleName: string
	remark: null
}

// 获取角色列表和用户已有角色列表接口返回数据 ts类型
export interface RoleListData {
	assignRoles: Role[]
	allRolesList: Role[]
}
