import _http from "@/request/http"
// 引入ts类型
import type { UserListData, Record, RoleListData } from "./type"
// 枚举api地址
enum API {
	// 获取用户列表
	USERLIST_URL = "/admin/acl/user/",
	// 新增用户
	ADDUSER_URL = "/admin/acl/user/save",
	// 修改用户
	EDITUSER_URL = "/admin/acl/user/update",
	// 删除用户
	DELETEUSER_URL = "/admin/acl/user/remove/",
	// 批量删除用户
	BATCHREMOVE_URL = "/admin/acl/user/batchRemove",
	// 获取角色列表和用户已有角色列表
	ROLELIST_URL = "/admin/acl/user/toAssign/",
	// 给用户分配角色
	ASSIGNROLE_URL = "/admin/acl/user/doAssignRole",
}

// 获取用户列表
export const reqUserList = <T extends { username: string }>(
	page: number,
	limit: number,
	params: T,
) => {
	if (params.username) {
		return _http.get<UserListData, T>(
			API.USERLIST_URL + `${page}/${limit}`,
			params,
		)
	} else {
		return _http.get<UserListData, any>(API.USERLIST_URL + `${page}/${limit}`)
	}
}

// 新增或修改用户
export const reqAddOrEditUser = (data: Record) => {
	if (data.id) {
		// 修改
		return _http.put<null, Record>(API.EDITUSER_URL, data)
	} else {
		// 新增
		return _http.post<null, Record>(API.ADDUSER_URL, data)
	}
}

// 删除用户
export const reqDeleteUser = <T>(id: T) =>
	_http.delete<null, any>(API.DELETEUSER_URL + id)

// 批量删除用户
export const reqBatchRemove = <
	// T extends number[],
	T,
>(
	data: T,
) => _http.delete<null, T>(API.BATCHREMOVE_URL, data)

// 获取角色列表和用户已有角色列表
export const reqRoleList = (id: number) =>
	_http.get<RoleListData, null>(API.ROLELIST_URL + id)

// 给用户分配角色
export const reqAssignRole = <T>(data: T) =>
	_http.post<any, T>(API.ASSIGNROLE_URL, data)
