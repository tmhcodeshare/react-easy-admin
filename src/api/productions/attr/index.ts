import _http from "@/request/http"

// 枚举接口地址
enum API {
	// 一级分类接口地址
	C1_URL = "/admin/product/getCategory1",
	// 二级分类接口地址
	C2_URL = "/admin/product/getCategory2/",
	// 三级分类接口地址
	C3_URL = "/admin/product/getCategory3/",
	// 获取商品属性接口地址
	ATTR_URL = "/admin/product/attrInfoList/",
	// 删除属性接口地址
	DELETEATTR_URL = "/admin/product/deleteAttr/",
	// 新增或编辑属性接口地址
	ADDOREDITATTR_URL = "/admin/product/saveAttrInfo",
}

// 引入类型
import type { CategoryOptions, Attr } from "./type"

// 获取一级分类接口
export const reqC1Options = () => _http.get<CategoryOptions[], null>(API.C1_URL)

// 获取二级分类接口
export const reqC2Options = (category1Id: number | string) =>
	_http.get<CategoryOptions[], null>(API.C2_URL + category1Id)

// 获取三级分类接口
export const reqC3Options = (category2Id: number | string) =>
	_http.get<CategoryOptions[], null>(API.C3_URL + category2Id)

// 获取商品属性接口
export const reqAttr = (
	category1Id: number | string,
	category2Id: number | string,
	category3Id: number | string,
) =>
	_http.get<Attr[], null>(
		API.ATTR_URL + `${category1Id}/${category2Id}/${category3Id}`,
	)

// 删除商品属性接口
export const reqDeleteAttr = (attrId: number) =>
	_http.delete<null, null>(API.DELETEATTR_URL + attrId)

// 新增或编辑属性接口
export const reqAddOrEditAttr = (data: Attr) =>
	_http.post<null, Attr>(API.ADDOREDITATTR_URL, data)
