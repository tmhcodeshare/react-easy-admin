// 分类列表每一项ts类型
export interface CategoryOptions {
	category1Id?: number
	category2Id?: number
	category3Id?: number
	id: number
	name: string
	createTime: string
	updateTime: string
}

// 定义单个属性值列表项ts类型
export interface AttrValueObj {
	attrId?: number
	createTime?: null
	id?: number
	updateTime?: null
	valueName: string
	preview?: boolean
}

// 定义属性列表ts类型
export type AttrValueList = AttrValueObj[]

// 定义单个属性ts类型
export interface Attr {
	attrName: string
	attrValueList: AttrValueList
	categoryId?: number
	categoryLevel: number
	createTime?: null
	id?: number
	updateTime?: null
	attrIdAndValueId?: undefined
}
/* {
  "attrName": "string",
  "attrValueList": [
    {
      "attrId": 0,
      "createTime": "2023-06-29T00:30:54.585Z",
      "id": 0,
      "updateTime": "2023-06-29T00:30:54.585Z",
      "valueName": "string"
    }
  ],
  "categoryId": 0,
  "categoryLevel": 0,
  "createTime": "2023-06-29T00:30:54.585Z",
  "id": 0,
  "updateTime": "2023-06-29T00:30:54.585Z"
} */
