import _http from "@/request/http"
import type { TradeMark, TradeMarkDate } from "./type"
// 枚举请求路径
enum API {
	TRADEMARKLIST_URL = "/admin/product/baseTrademark/",
	ADDTRADEMARK_URL = "/admin/product/baseTrademark/save",
	UPDATETRADEMARK_URL = "/admin/product/baseTrademark/update",
	DELETETRADEMARK_URL = "/admin/product/baseTrademark/remove/",
}

// 获取品牌列表
export const reqTradeMarkList = (page: number, limit: number) =>
	_http.get<TradeMark, null>(API.TRADEMARKLIST_URL + `${page}/${limit}`)

// 新增或修改品牌
export const reqAddOrUpdateTrademark = (data: TradeMarkDate) => {
	if (data.id) {
		// 修改
		return _http.put<null, TradeMarkDate>(API.UPDATETRADEMARK_URL, data)
	} else {
		// 新增
		return _http.post<null, TradeMarkDate>(API.ADDTRADEMARK_URL, data)
	}
}

//删除品牌接口
export const reqDelateTradeMark = (id: number) =>
	_http.delete<string, null>(API.DELETETRADEMARK_URL + id)
