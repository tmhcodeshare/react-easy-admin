// 单个Record ts类型
export interface TradeMarkDate {
	id?: number
	createTime?: string
	logoUrl: string
	tmName: string
	updateTime?: string
}

// Records ts类型
export type Records = TradeMarkDate[]

// 获取品牌列表接口返回数据ts类型
export interface TradeMark {
	countId: null | string
	current: number
	hitCount: boolean
	maxLimit: null
	optimizeCountSql: boolean
	orders: []
	pages: number
	records: Records
	searchCount: boolean
	size: number
	total: number
}
