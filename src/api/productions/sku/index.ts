import _http from "@/request/http"
// 引入ts类型
import { SKU, SkuListData } from "./type"
// 枚举请求地址
enum API {
	// 添加sku
	ADDSKU_URL = "/admin/product/saveSkuInfo",
	// 根据SPUID获取sku列表
	SKULISTBYSPUID_URL = "/admin/product/findBySpuId/",
	// 获取sku列表
	SKULIST_URL = "/admin/product/list/",
	// 查询sku详情
	SKUDETAIL_URL = "/admin/product/getSkuInfo/",
	// 删除sku
	DELETESKU_URL = "/admin/product/deleteSku/",
	// 商品上架
	ONSALE_URL = "/admin/product/onSale/",
	// 商品下架
	CANCELSALE_URL = "/admin/product/cancelSale/",
}

// 添加sku
export const reqAddSku = (data: SKU) =>
	_http.post<null, SKU>(API.ADDSKU_URL, data)

// 根据SPUID获取sku列表
export const reqSkuListBySpuId = (spuId: number) =>
	_http.get<SKU[], null>(API.SKULISTBYSPUID_URL + spuId)

// 获取SKU列表
export const reqSkuList = (page: number, limt: number) =>
	_http.get<SkuListData, null>(API.SKULIST_URL + `${page}/${limt}`)

// 查询sku详情
export const reqSkuDeatil = (skuId: number) =>
	_http.get<SKU, null>(API.SKUDETAIL_URL + skuId)

// 查询sku详情
export const reqDeleteSku = (skuId: number) =>
	_http.get<null, null>(API.DELETESKU_URL + skuId)

// 商品上架
export const reqOnSale = (skuId: number) =>
	_http.get<null, null>(API.ONSALE_URL + skuId)

// 商品上架
export const reqCancelSale = (skuId: number) =>
	_http.get<null, null>(API.CANCELSALE_URL + skuId)
