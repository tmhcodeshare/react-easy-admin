// sku平台属性 ts类型
export interface SkuAttr {
	attrId: number
	valueId: number
	attrName?: string
	createTime?: string
	id?: number
	skuId?: number
	updateTime?: string
	valueName?: string
}

// sku销售属性 ts类型
export interface SkuSaleAttr {
	saleAttrId: number
	saleAttrValueId: number
	id?: number
	saleAttrName?: string
	saleAttrValueName?: string
	skuId?: number
	spuId?: number
	createTime?: string
	updateTime?: string
}
// sku图片 ts类型
export interface SkuImg {
	id?: number
	imgName: string
	imgUrl: string
	isDefault?: string
	skuId?: number
	spuImgId?: number
	createTime?: string
	updateTime?: string
}
// sku ts类型
export interface SKU {
	category3Id: number | undefined // 三级分类id
	spuId: number | undefined // 已有SPUid
	tmId: number | undefined // 品牌id
	price: number | undefined // 价格
	weight: string // 重量
	skuDesc: string // sku描述
	skuDefaultImg: string // sku默认照片
	skuName: string // sku名称
	id?: number // skuid
	isSale?: number
	skuAttrValueList?: SkuAttr[] | null
	skuSaleAttrValueList?: SkuSaleAttr[] | null
	skuImageList?: SkuImg[] | null
	createTime?: string
	updateTime?: string
}

// 获取sku列表接口 ts类型
export interface SkuListData {
	records: SKU[]
	total: number
	size: number
	current: number
	orders: []
	optimizeCountSql: boolean
	hitCount: boolean
	countId: null
	maxLimit: null
	searchCount: boolean
	pages: number
}
