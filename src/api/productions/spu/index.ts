import _http from "@/request/http"
import type {
	Spu,
	HasSpuListData,
	TradeMark,
	ImageObj,
	BaseAttr,
	SaleAttr,
} from "./type"
// 枚举接口
enum API {
	// 获取已有SPU列表
	HASSPULIST_URL = "/admin/product/",
	// 获取全部品牌列表
	ALLTRADEMARKLIST_URL = "/admin/product/baseTrademark/getTrademarkList",
	// 获取SPU照片列表
	IMAGELIST_URL = "/admin/product/spuImageList/",
	// 获取该项目全部销售属性（颜色、版本、尺码）
	ALLBASESALEATTRLIST_URL = "/admin/product/baseSaleAttrList",
	// 获取详细的销售属性列表
	SALEATTRLIST_URL = "/admin/product/spuSaleAttrList/",
	// 新增SPU
	ADDSPU_URL = "/admin/product/saveSpuInfo",
	// 编辑已有SPU
	EDITSPU_URL = "/admin/product/updateSpuInfo",
	// 删除SPU
	DELETESPU_URL = "/admin/product/deleteSpu/",
}

// 获取已有SPU列表接口
export const reqHasSpuList = (
	page: number,
	limit: number,
	category3Id: number | undefined,
) =>
	_http.get<HasSpuListData, null>(
		API.HASSPULIST_URL + `${page}/${limit}?category3Id=${category3Id}`,
	)

// 获取全部品牌列表接口
export const reqAllTradeMarkList = () =>
	_http.get<TradeMark[], null>(API.ALLTRADEMARKLIST_URL)

// 获取SPU照片列表接口
export const reqImageList = (spuId: number) =>
	_http.get<ImageObj[], null>(API.IMAGELIST_URL + spuId)

// 获取该项目全部销售属性（颜色、版本、尺码）接口
export const reqAllBaseSaleAttrList = () =>
	_http.get<BaseAttr[], null>(API.ALLBASESALEATTRLIST_URL)

// 获取详细的销售属性列表接口
export const reqSaleAttrList = (spuId: number) =>
	_http.get<SaleAttr[], null>(API.SALEATTRLIST_URL + spuId)

// 新增SPU
export const reqAddSpu = (data: Spu) =>
	_http.post<null, Spu>(API.ADDSPU_URL, data)

// 编辑已有SPU
export const reqEditSpu = (data: Spu) =>
	_http.post<null, Spu>(API.EDITSPU_URL, data)

// 删除已有的SPU
export const reqDeleteSpu = (spuId: number) =>
	_http.delete<null, null>(API.DELETESPU_URL + spuId)
