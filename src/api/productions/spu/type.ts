// 定义单个SPU ts类型
export interface Spu {
	id: number | string // spu属性对象id
	spuName: string // spu属性名称
	tmId: number | string // 品牌id
	category3Id: number | string // 三级分类id
	description: string // 品牌描述
	spuImageList: null | ImageObj[] // 照片墙
	spuSaleAttrList: null | SaleAttr[] // 销售属性
	createTime?: string
	updateTime?: string
	spuPosterList?: null
}

// 定义SPU列表 ts类型
export type Records = Spu[]

// 定义获取已有SPU列表接口返回数据ts类型
export interface HasSpuListData {
	records: Records
	total: number
	size: number
	current: number
	orders: []
	optimizeCountSql: boolean
	hitCount: boolean
	countId: null
	maxLimit: null
	searchCount: boolean
	pages: number
}

// 定义单个TradeMark ts类型
export interface TradeMark {
	id: number
	createTime?: string
	updateTime?: string
	tmName: string
	logoUrl: string
}

// 定义单个image ts类型
export interface ImageObj {
	id?: number
	imgName?: string
	imgUrl?: string
	createTime?: string
	updateTime?: string
	spuId?: number
	name?: string
	url?: string
}
// 定义单个基础属性 ts类型
export interface BaseAttr {
	id: number
	createTime?: string
	updateTime?: string
	name: string
}

// 定义单个销售属性属性值 TS类型
export interface SaleAttrValue {
	id?: number
	createTime?: null
	updateTime?: null
	spuId?: number
	baseSaleAttrId: number // 销售属性值id
	saleAttrValueName: string | undefined // 销售属性值名称
	saleAttrName?: string
	isChecked?: null
}

// 定义销售属性属性值列表  ts类型
export type SaleAttrValueList = SaleAttrValue[]

// 定义销售属性 TS类型
export interface SaleAttr {
	id?: number // 当前销售属性id
	createTime?: null
	updateTime?: null
	spuId?: number // 品牌spuid
	baseSaleAttrId: number // 基础销售属性id
	saleAttrName: string // 销售属性名称
	spuSaleAttrValueList: SaleAttrValueList // 销售属性属性值列表
	flag?: boolean // 控制收集添加按钮与输入框显示隐藏的属性
	saleAttrValueName?: string // 收集的属性值名称
	saleAttrIdAndSaleAttrValueId?: string // 表单收集数据临时字段 baseSaleAttrId id
}
