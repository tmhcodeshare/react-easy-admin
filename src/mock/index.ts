import Mock from "mockjs"

Mock.mock("/api/admin/acl/index/login", "post", (req: any) => {
  const t = "admin-token"
	return {
		code: 200,
		data: t,
		message: "操作成功",
	}
})
