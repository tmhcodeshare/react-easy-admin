import React, { useEffect } from "react"
import { ConfigProvider, theme } from "antd"
import { useAppSelector } from "./store"
import { shallowEqual } from "react-redux"
import zhCN from "antd/locale/zh_CN"
import ErrorBoundary from "@/components/ErrorBoundary"
import AuthRouter from "@/router/utils/AouthRouter"
import Router from "@/router"
import "./App.less"

function App() {
	return (
		<>
			<ErrorBoundary>
				<AuthRouter>
					<Router />
				</AuthRouter>
			</ErrorBoundary>
		</>
	)
}

function Root() {
	const { themeKey, primary } = useAppSelector(
		(state) => ({
			themeKey: state.layoutStore.themeKey,
			primary: state.layoutStore.primary,
		}),
		shallowEqual,
	)

	useEffect(() => {
		const body = document.body
		const flag = Array.from(body.classList).includes("dark")
		themeKey === "light"
			? flag && body.classList.remove("dark")
			: !flag && body.classList.add("dark")
	}, [themeKey])

	return (
		<ConfigProvider
			theme={{
				algorithm:
					themeKey === "light" ? theme.defaultAlgorithm : theme.darkAlgorithm,
				token: {
					colorPrimary: primary,
				},
			}}
			locale={zhCN}
		>
			<App />
		</ConfigProvider>
	)
}

export default Root
