import { useRoutes } from "react-router-dom"
import type { RouteObject } from "./type"
import { constanceRoutes, dynamicRoutes, anyRoutes } from "./routers"
export const routers: RouteObject[] = [
	...constanceRoutes,
	...dynamicRoutes,
	...anyRoutes,
]
const Router = () => {
	const routes = useRoutes(routers)

	return routes
}
export default Router
