// import { useLocation, useNavigate } from "react-router-dom"
// import { useAppSelector } from "@/store"
// import {
// 	saveUserInfoAction,
// 	saveDynamicRouteListAction,
// } from "@/store/modules/user"
// import { routers } from "@/router"
// import { shallowEqual, useDispatch } from "react-redux"
// import { useEffect } from "react"
import type { FC } from "react"
// import { reqUserInfo } from "@/api/user"
// import { filterRoutes, findRoute, getTimeRegion } from "@/utils"
// import { dynamicRoutes } from "@/router/routers"
// import { notification } from "antd"
// import { RouteObject } from "../type"
// const cloneDeep = require("lodash/cloneDeep")
interface Iprops {
	children?: any
}
// // 静态路由
// const constance = ["/login", "/", "/home", "/screen", "/404"].map((path) => {
// 	return findRoute(path, routers)
// })
// // 任意路由
// const anyroute = ["*"].map((path) => findRoute(path, routers))
import { usePermission } from "@/hooks/usePermission"
/**
 * @description 路由权限组件
 * */
const AuthRouter: FC<Iprops> = (props) => {
	usePermission()
	return props.children
}

export default AuthRouter
