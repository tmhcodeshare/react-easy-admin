import { lazy } from "react"
import { Navigate } from "react-router-dom"
import Layout from "@/Layout"
import lazyLoad from "./utils/lazyLoad"
import type { RouteObject } from "./type"
import Login from "@/views/Login"
export const constanceRoutes: RouteObject[] = [
	{
		path: "/login",
		element: <Login />,
		meta: {
			requiresAuth: false,
			title: "登录",
			key: "login",
			hiden: true,
		},
	},
	{
		path: "/",
		element: <Navigate to="/home" replace={true} />,
		meta: {
			hiden: true,
			title: "首页",
		},
	},
	{
		element: <Layout />,
		children: [
			{
				path: "/home",
				element: lazyLoad(lazy(() => import("@/views/Home"))),
				meta: {
					requiresAuth: true,
					title: "首页",
					key: "home",
					icon: "HomeOutlined",
					hiden: false,
				},
			},
		],
	},
	{
		path: "/screen",
		element: lazyLoad(lazy(() => import("@/views/Screen"))),
		meta: {
			requiresAuth: true,
			title: "数据大屏",
			key: "screen",
			icon: "AreaChartOutlined",
			hiden: false,
		},
	},
	{
		path: "/404",
		element: lazyLoad(lazy(() => import("@/views/404"))),
		meta: {
			requiresAuth: false,
			hiden: true,
			title: "404",
		},
	},
	{
		element: <Layout />,
		meta: {
			requiresAuth: false,
			title: "上传管理",
			key: "upload",
			icon: "ShopOutlined",
			hiden: false,
		},
		children: [
			{
				path: "/upload/",
				element: lazyLoad(lazy(() => import("@/views/Upload"))),
				meta: {
					requiresAuth: true,
					title: "文件上传",
					key: "uploadFile",
					icon: "HomeOutlined",
					hiden: false,
				},
			},
		],
	},
]

export const dynamicRoutes: RouteObject[] = [
	{
		element: <Layout />,
		meta: {
			requiresAuth: true,
			title: "权限管理",
			key: "acl",
			icon: "LockOutlined",
			hiden: false,
			name: "Acl",
		},
		children: [
			{
				path: "/acl/permission",
				element: lazyLoad(lazy(() => import("@/views/Acl/Permission"))),
				meta: {
					requiresAuth: true,
					title: "菜单权限",
					key: "permission",
					icon: "AppstoreOutlined",
					hiden: false,
					name: "Permission",
				},
			},
			{
				path: "/acl/role",
				element: lazyLoad(lazy(() => import("@/views/Acl/Role"))),
				meta: {
					requiresAuth: true,
					title: "角色权限",
					key: "role",
					icon: "TeamOutlined",
					hiden: false,
					name: "Role",
				},
			},
			{
				path: "/acl/user",
				element: lazyLoad(lazy(() => import("@/views/Acl/User"))),
				meta: {
					requiresAuth: true,
					title: "用户权限",
					key: "user",
					icon: "UserOutlined",
					hiden: false,
					name: "User",
				},
			},
		],
	},
	{
		element: <Layout />,
		meta: {
			requiresAuth: true,
			title: "商品管理",
			key: "product",
			icon: "ShopOutlined",
			hiden: false,
			name: "Product",
		},
		children: [
			{
				path: "/product/attr",
				element: lazyLoad(lazy(() => import("@/views/Product/Attr"))),
				meta: {
					requiresAuth: true,
					title: "属性管理",
					key: "attr",
					icon: "SettingOutlined",
					hiden: false,
					name: "Attr",
				},
			},
			{
				path: "/product/spu",
				element: lazyLoad(lazy(() => import("@/views/Product/Spu"))),
				meta: {
					requiresAuth: true,
					title: "Spu管理",
					key: "spu",
					icon: "ShoppingOutlined",
					hiden: false,
					name: "Spu",
				},
			},
			{
				path: "/product/sku",
				element: lazyLoad(lazy(() => import("@/views/Product/Sku"))),
				meta: {
					requiresAuth: true,
					title: "Sku管理",
					key: "sku",
					icon: "SkinOutlined",
					hiden: false,
					name: "Sku",
				},
			},
			{
				path: "/product/trademark",
				element: lazyLoad(lazy(() => import("@/views/Product/TradeMark"))),
				meta: {
					requiresAuth: true,
					title: "品牌管理",
					key: "trademark",
					icon: "ShoppingCartOutlined",
					hiden: false,
					name: "Trademark",
				},
			},
		],
	},
]

export const anyRoutes: RouteObject[] = [
	{
		path: "*",
		element: <Navigate to="/404" replace={true} />,
		meta: {
			hiden: true,
			title: "any",
		},
	},
]
