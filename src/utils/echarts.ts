// echarts 按需导入模块
// 引入并导出 echarts 核心模块，核心模块提供了 echarts 使用必须要的接口。
import * as echarts from "echarts/core"
// 引入柱状图图表，图表后缀都为 Chart
import {
	BarChart,
	BarSeriesOption,
	PieChart,
	PieSeriesOption,
	LineChart,
	LineSeriesOption,
	LinesChart,
	LinesSeriesOption,
	MapChart,
	MapSeriesOption,
} from "echarts/charts"
// 引入提示框，标题，直角坐标系，数据集，内置数据转换器组件，组件后缀都为 Component
import {
	TitleComponent,
	TitleComponentOption,
	TooltipComponent,
	TooltipComponentOption,
	GridComponent,
	GridComponentOption,
	GeoComponent,
	GeoComponentOption,
	PolarComponent,
	PolarComponentOption,
	LegendComponent,
	LegendComponentOption,
	DatasetComponent,
	DatasetComponentOption,
	TransformComponent,
} from "echarts/components"
// 引入并导出 标签自动布局、全局过渡动画等特性
import { LabelLayout, UniversalTransition } from "echarts/features"
// 引入并导出 Canvas 渲染器，注意引入 CanvasRenderer 或者 SVGRenderer 是必须的一步
import { CanvasRenderer } from "echarts/renderers"

// 通过 ComposeOption 来组合出一个只有必须组件和图表的 Option 类型
export type ECOption = echarts.ComposeOption<
	| BarSeriesOption
	| PieSeriesOption
	| LineSeriesOption
	| LinesSeriesOption
	| MapSeriesOption
	| TitleComponentOption
	| TooltipComponentOption
	| GridComponentOption
	| GeoComponentOption
	| PolarComponentOption
	| LegendComponentOption
	| DatasetComponentOption
>

// 注册必须的组件
echarts.use([
	TitleComponent,
	TooltipComponent,
	GridComponent,
	GeoComponent,
	PolarComponent,
	LegendComponent,
	DatasetComponent,
	TransformComponent,
	BarChart,
	PieChart,
	LineChart,
	LinesChart,
	MapChart,
	LabelLayout,
	UniversalTransition,
	CanvasRenderer,
])

export default echarts
