import SparkMD5 from "spark-md5"
// import axios from "axios"
let count = { count: 0 }
let controllers = []
self.addEventListener("message", ({ data }) => {
	console.log("sliceUploadWork", data)
	const { file, url, sliceSize, type } = data
	if (type === "start") {
		const fileReader = new FileReader()
		fileReader.readAsArrayBuffer(file)
		const spark = new SparkMD5.ArrayBuffer()
		const chunkSize = sliceSize
		let fillname = ""
		console.log("chunkSize", chunkSize)
		fileReader.onload = (e) => {
			// 根据文件内容生成md5作为文件名
			spark.append(e.target.result)
			const md5 = spark.end()
			console.log("md5", md5)
			fillname = md5
			// 文件切片
			const chunks = []
			const totalChunks = Math.ceil(file.size / chunkSize)
			for (let i = 0; i < totalChunks; i++) {
				const start = i * chunkSize
				const end = Math.min(start + chunkSize, file.size)
				const chunk = file.slice(start, end)
				chunks.push(chunk)
			}
			// postMessage({ fillname, chunks })
			console.log("chunks", chunks)
			// count如果直接传number类型，那么, uploadSlice中形参count属性值和外界传入的count的值会不同步。
			// 这是因为，外界count传入的值只作为初始值，后续uploadSlice累加的一直是形参count的属性值
			// 所以，传入的count需要作为对象传入，这样uploadSlice中形参count的属性值和外界传入的count的值才会指向同一个地址，从而同步
			uploadSlice({
				url,
				chunks,
				fillname,
				count,
				postMessage: self.postMessage,
				type: file.type,
				controllers,
			})
		}
	} else {
		console.log("abort count", count.count)
		const controller = controllers[count.count]
		console.log("controller", controller)
		console.log("controllers", controllers)
		console.log("abort", controller.abort)
		// abort方法的调用对象必须是AbortController的实例
		controller.abort && controller.abort()
	}
})

function uploadSlice({
	url,
	chunks,
	fillname,
	count,
	postMessage,
	type,
	controllers,
}) {
	console.log("uploadSlice count", count.count)
	if (count.count >= chunks.length) {
		fetch("http://localhost:6061/api/upload/merge", {
			method: "post",
			body: JSON.stringify({
				filename: fillname,
				type,
			}),
			headers: {
				"Content-Type": "application/json",
			},
		})
			.then((res) => res.json())
			.then((res) => {
				postMessage({
					precent: 100,
					url: "http://localhost:6061/api" + res.data.url,
				})
			})
		return
	}
	const formData = new FormData()
	formData.append(
		"slice",
		chunks[count.count],
		fillname + "-" + (count.count + 1),
	)
	const controller = new AbortController()
	controllers[count.count] = controller
	// 上传切片
	fetch(url, {
		method: "post",
		body: formData,
		signal: controller.signal,
	})
		.then((res) => res.json())
		.then((res) => {
			console.log("res", res)
			count.count++
			postMessage({ precent: (count.count / chunks.length) * 100 })
			uploadSlice({
				url,
				chunks,
				fillname,
				count,
				postMessage,
				type,
				controllers,
			})
		})
}
