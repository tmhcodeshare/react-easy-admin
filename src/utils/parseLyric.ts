interface Lyric {
	time: number
	text: string
}
const tiemRegx = /\[(\d{2}):(\d{2})\.(\d{2,3})\]/
export const parseLyric = (lyric: string): Lyric[] => {
	// 分割歌词。拿到每行歌词
	const lines = lyric.split("\n")
	const lyrics: Lyric[] = []
	for (let i = 0; i < lines.length; i++) {
		const item = lines[i]
		const timeResult = item.match(tiemRegx)
		if (!timeResult) continue
		const time1 = Number(timeResult[1]) * 60 * 1000
		const time2 = Number(timeResult[2]) * 1000
		const time3 =
			timeResult[3].length === 2
				? Number(timeResult[3]) * 10
				: Number(timeResult[3])
		const time = time1 + time2 + time3
		const text = item.replace(tiemRegx, "")
		lyrics.push({ time, text })
	}
	return lyrics
}
