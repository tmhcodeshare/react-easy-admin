// 存数据
export const setItem = (key: string, str: string) => {
	sessionStorage.setItem(key, str)
}

// 取数据
export const getItem = (key: string): string => {
	return sessionStorage.getItem(key) as string
}

// 删数据
export const removeItem = (key: string) => {
	sessionStorage.removeItem(key)
}
