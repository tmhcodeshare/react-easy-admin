// 规范图片url
export const formatImageUrl = (
	url: string,
	width: number,
	height: number = width,
): string => {
	return url + `?param=${width}y${height}`
}

// 规范播放量
export const formatCount = (count: number) => {
	return count > 100000 ? Math.floor(count / 10000) + "万" : count
}

// 分割数组
export const ArraySlice = <T>(arr: T[], size: number): Array<Array<T>> => {
	const count = Math.ceil(arr.length / size)
	let sign = 0
	return arr
		.map((item: T, index) => {
			if (sign > count) return
			if (index % size === 0) {
				const newArr = arr.slice(sign * size, (sign + 1) * size).filter(Boolean)
				sign += 1
				return newArr
			}
		})
		.filter(Boolean) as Array<Array<T>>
}
// 格式化时间
export function formatTime(time: number) {
	// 1.将毫秒转成秒钟
	const timeSeconds = time / 1000

	// 2.获取分钟和秒钟
	// 100s => 01:40
	// 200s => 03:20
	// Math.floor(100 / 60) => 1
	const minute = Math.floor(timeSeconds / 60)
	const second = Math.floor(timeSeconds) % 60

	// 3.格式化时间
	const formatMinute = String(minute).padStart(2, "0")
	const formatSecond = String(second).padStart(2, "0")

	return `${formatMinute}:${formatSecond}`
}

// 获取当前歌曲url
export function getSongPlayUrl(id: number) {
	return `https://music.163.com/song/media/outer/url?id=${id}.mp3`
}
