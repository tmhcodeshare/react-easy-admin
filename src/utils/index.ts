import type { RouteObject } from "@/router/type"
// 获取路由
export const findRoute = (url: string, arr: RouteObject[]): RouteObject => {
	let result: RouteObject = {}
	arr.forEach((item) => {
		if (item.path && item.path === url) result = item
		if (item.children) {
			const res = findRoute(url, item.children)
			if (Object.keys(res).length) result = res
		}
	})
	return result
}
// 筛选路由
export const filterRoutes = (
	routes: RouteObject[],
	permissions: string[],
): RouteObject[] => {
	return routes.filter((item) => {
		if (item?.meta?.name && permissions.includes(item.meta?.name)) {
			if (item.children) {
				item.children = filterRoutes(item.children, permissions)
			}
			return true
		}
	})
}
/**
 * getTimeRegion函数用于定义当前时间处于哪个时间段（早上、上午、下午、晚上）
 * @return string
 */
export const getTimeRegion = (): string => {
	let region = ""
	const h = new Date().getHours()
	if (h >= 0 && h < 9) {
		region = "早上"
	} else if (h >= 9 && h < 12) {
		region = "上午"
	} else if (h >= 12 && h < 18) {
		region = "下午"
	} else {
		region = "晚上"
	}
	return region
}

/**
 * @description 生成随机数
 * @param {Number} min 最小值
 * @param {Number} max 最大值
 * @return number
 */
export function randomNum(min: number, max: number): number {
	let num = Math.floor(Math.random() * (min - max) + max)
	return num
}

// 判断独享是否含有指定属性
const hasOwnProperty = (obj: object, key: string) => {
	return Object.prototype.hasOwnProperty.call(obj, key)
}
// 判断值类型
const typeStr = (obj: any) => {
	return Object.prototype.toString.call(obj).replace(/\[|\]/g, "").slice(7)
}
// 比较数组
const compareArr = (
	arr1: Array<any>,
	arr2: Array<any>,
	deepCompare: Function,
) => {
	if (arr1.length !== arr2.length) return false
	for (let i = 0; i < arr1.length; i++) {
		if (typeStr(arr1[i]) !== typeStr(arr2[i])) return false
		if (["Object", "Array"].includes(typeStr(arr1[i])))
			return deepCompare(arr1[i], arr2[i])
		if (!Object.is(arr1[i], arr2[i])) return false
	}
	return true
}
// 比较对象
const compareObj = (obj1: any, obj2: any, deepCompare: Function) => {
	const keys1 = Object.keys(obj1)
	const keys2 = Object.keys(obj2)
	if (keys1.length !== keys2.length) return false
	for (let i = 0; i < keys1.length; i++) {
		const key = keys1[i]
		if (!hasOwnProperty(obj2, key)) return false
		if (typeStr(obj1[key] !== typeStr(obj2[key]))) return false
		if (["Object", "Array"].includes(typeStr(obj1[key])))
			return deepCompare(obj1[key], obj2[key])
		if (!Object.is(obj1[key], obj2[key])) return false
	}
	return true
}
// 深层次比较
/* export function deepCompare(obj1: any, obj2: any): boolean {
	const keys1 = Object.keys(obj1)
	const keys2 = Object.keys(obj2)
	if (keys1.length !== keys2.length) return false
	for (let i = 0; i < keys1.length; i++) {
		const key = keys1[i]
		if (!hasOwnProperty(obj2, key)) return false
		if (!typeEqul(obj1[key], obj2[key])) return false
		if (typeof obj1[key] === "object") return deepCompare(obj1[key], obj2[key])
		return Object.is(obj1[key], obj2[key])
	}
	return Object.is(obj1, obj2)
} */

export function deepCompare(obj1: any, obj2: any): boolean {
	const type1 = typeStr(obj1)
	const type2 = typeStr(obj2)

	if (type1 !== type2) return false
	if (type1 === "Array") {
		return compareArr(obj1, obj2, deepCompare)
	}
	if (type1 === "Object") {
		return compareObj(obj1, obj2, deepCompare)
	}
	return Object.is(obj1, obj2)
}
