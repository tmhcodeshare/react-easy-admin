import React from "react"
import ReactDOM from "react-dom/client"
import { HashRouter } from "react-router-dom"
import { Provider } from "react-redux"
import { PersistGate } from "redux-persist/integration/react"
import store, { persistor } from "@/store"
import { AliveScope } from "react-activation"
// import "@/mock"
import "@/styles/less/reset.less"
// import "@/styles/less/theme.less"
import "@/index.less"
import App from "@/App"
const root = ReactDOM.createRoot(document.getElementById("root") as HTMLElement)
root.render(
	<HashRouter>
		<Provider store={store}>
			{/* redux持久化 */}
			<PersistGate persistor={persistor}>
				<AliveScope>
					<App />
				</AliveScope>
			</PersistGate>
		</Provider>
	</HashRouter>,
)
