// async function async1() {
// 	console.log("async1 start")
// 	await async2()
// 	console.log("async1 end")
// }
// async function async2() {
// 	console.log("async2 start")
// 	await async3()
// 	return new Promise((resolve) => {
// 		resolve()
// 		console.log("async2 promise")
// 	})
// }
// async function async3() {
// 	console.log("async3 start")
// 	return new Promise((resolve) => {
// 		resolve()
// 		console.log("async3 promise")
// 	})
// }
// console.log("script start")
// setTimeout(function () {
// 	console.log("setTimeout")
// }, 0)
// async1()
// new Promise(function (resolve) {
// 	console.log("promise1")
// 	resolve()
// })
// 	.then(function () {
// 		console.log("promise2")
// 	})
// 	.then(function () {
// 		console.log("promise3")
// 	})
// 	.then(function () {
// 		console.log("promise4")
// 	})
// 	.then(function () {
// 		console.log("promise5")
// 	})
// 	.then(function () {
// 		console.log("promise6")
// 	})
// console.log("script end")
Promise.resolve()
	.then(() => {
		console.log(0)
		return Promise.resolve(4)
	})
	.then((res) => {
		console.log(res)
	})

Promise.resolve()
	.then(() => {
		console.log(1)
	})
	.then(() => {
		console.log(2)
	})
	.then(() => {
		console.log(3)
	})
	.then(() => {
		console.log(4)
	})
	.then(() => {
		console.log(5)
	})
